#![allow(clippy::cast_lossless)]

use common::{Bytes, RABytes};

#[cfg(test)]
mod tests;

#[derive(Debug)]
struct FileIndex {
    file_offset: u32,
    file_lenght: u32,
}

#[derive(Debug)]
pub struct CPack {
    offset_table: Vec<FileIndex>,
    file: Bytes,
}

impl CPack {
    pub fn new_from_bytes(file: Bytes) -> Result<CPack, &'static str> {
        let mut result = CPack{
            offset_table: Vec::new(),
            file,
        };
        result.parse()?;
        Ok(result)
    }

    pub fn parse(&mut self) -> Result<(), &'static str> {
        self.file.seek(0);
        if self.file.read(4)? != vec![0,0,0,0] {
            return Err("The 4 first bytes aren't 0s!!!")
        };
        let number_of_file = self.file.read_u32_le()?;
        for _file_id in 0..number_of_file {
            let file_offset = self.file.read_u32_le()?;
            let file_lenght = self.file.read_u32_le()?;
            if file_offset + file_lenght > self.file.len() as u32 {
                return Err("a file is out of the scope in a CPack!!!");
            }
            self.offset_table.push(FileIndex {
                file_offset, file_lenght,
            });
        }
        if self.file.read(8)? != vec![0,0,0,0,0,0,0,0] {
            return Err("The 8 bytes at the end of the TOC aren't 0s.");
        }
        Ok(())
    }

    pub fn len(&self) -> usize {
        self.offset_table.len()
    }

    pub fn is_empty(&self) -> bool {
        self.len() == 0
    }

    /// get the file by an id, and return it as Bytes. panic if it doesn't exist
    pub fn get_file(&self, id: usize) -> Bytes {
        let file_data = &self.offset_table[id];
        self.file.create_sub_file(
            file_data.file_offset as u64,
            file_data.file_lenght as u64
        ).unwrap() //should not fail
    }
}

#[derive(Debug, Default)]
/// A structure that allow to create a CPack file
pub struct CPackCreator<B: RABytes> {
    files: Vec<B>,
}

impl<B: RABytes> CPackCreator<B> {
    /// add a file to the cpack
    pub fn push(&mut self, file: B) {
        self.files.push(file);
    }
    /// transform the actual content of the [CPackCreator] to a cpack file
    pub fn write(&self) -> Result<Bytes, &'static str> {
        let mut file = Bytes::new();
        file.write_u32_le(0)?;
        file.write_u32_le(self.files.len() as u32)?;
        // file info. Need to be rewritten
        let mut nb = 0;
        for _ in 0..self.files.len() {
            file.write_u32_le(0)?;
            file.write_u32_le(0)?;
            nb += 8;
        };

        // seem to be a padding to 32 bytes
        while nb%32 != 0 {
            file.write_u8_le(0)?;
            nb += 1;
        };

        // another padding to 64 bytes (maybe 128)
        while file.tell()%64 != 0 {
            file.write_u8_le(0xFF)?;
        };


        let mut file_info = vec![];
        for f in &self.files {
            file_info.push(FileIndex {
                file_offset: file.tell() as u32,
                file_lenght: f.len() as u32,
            });
            file.write_bytes(f)?;
            // padding with the len of 16
            let mut nb = f.len();
            while nb%16 != 0 {
                file.write_u8_le(0xFF)?;
                nb += 1;
            }
        };

        file.seek(8);

        for info in file_info {
            file.write_u32_le(info.file_offset)?;
            file.write_u32_le(info.file_lenght)?;
        }

        Ok(file)
    }
}
