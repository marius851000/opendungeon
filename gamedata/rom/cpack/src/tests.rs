use crate::CPack;
use common::Bytes;

#[test]
fn test_cpack_read() {
    let buf = Bytes::new_from_vec(
        vec![0,0,0,0, //0-the magic
            2,0,0,0, //4-the number of element
            32,0,0,0,5,0,0,0, //8-the offset and the lenght of the first element
            37,0,0,0,5,0,0,0, //16-idem for the second element
            0,0,0,0,0,0,0,0, //24-magic
            104,101,108,108,111, //32-b"hello"
            119,111,114,108,100, //37-b"world"
            ],
    );
    let pack = CPack::new_from_bytes(buf).unwrap();
    assert_eq!(pack.len(), 2);
    assert_eq!(pack.get_file(0).as_string(), String::from("hello"));
    assert_eq!(pack.get_file(1).as_string(), String::from("world"));
}
