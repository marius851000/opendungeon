use std::path::PathBuf;
use std::fs;
use clap::{App, Arg, SubCommand};

use common::{Bytes, RABytes};
use opendungeon_rom_cpack::{CPack, CPackCreator};
use ndsrom::NdsRom;

fn main() {
    let matches = App::new("cpack")
        .subcommand(SubCommand::with_name("test")
            .about("try to decompress and recompress known cpack file in the EOS rom, and compare if thay are identical")
            .arg(Arg::with_name("rompath")
                .short("r")
                .value_name("rompath")
                .required(true))
        )
        .subcommand(SubCommand::with_name("unpack")
            .about("unpack all the files to the target folder (create if unexistant)")
            .arg(Arg::with_name("output")
                .short("o")
                .value_name("output")
                .required(true)
            )
            .arg(Arg::with_name("input")
                .short("i")
                .value_name("input")
                .required(true)
            )
        )
        .subcommand(SubCommand::with_name("pack")
            .about("pack all the files in the input folder")
            .arg(Arg::with_name("output")
                .short("o")
                .value_name("output")
                .required(true)
            )
            .arg(Arg::with_name("input")
                .short("i")
                .value_name("input")
                .required(true)
            )
        )
        .get_matches();


    if matches.subcommand_matches("test").is_some() {
        let subcommand = matches.subcommand_matches("test").unwrap();
        let rom = NdsRom::new_from_string(subcommand.value_of("rompath").unwrap());
        rom_test(rom);
    } else if matches.subcommand_matches("unpack").is_some() {
        let subcommand = matches.subcommand_matches("unpack").unwrap();
        let input_path = PathBuf::from(subcommand.value_of("input").unwrap());
        let output_path = PathBuf::from(subcommand.value_of("output").unwrap());

        match fs::metadata(&output_path) {
            Ok(value) => if value.is_file() {
                panic!("The targeted output folder ({:?}) is a file.", output_path);
            },
            Err(_) => {
                println!("The output folder can't be accessed. Creating it.");
                fs::DirBuilder::new()
                    .recursive(true)
                    .create(&output_path).unwrap();
            }
        }

        println!("reading the input file...");
        let input_file = Bytes::new_from_path(&input_path).unwrap();
        let input_cpack = CPack::new_from_bytes(input_file).unwrap();

        println!("writing the outputs file...");
        for file_id in 0..input_cpack.len() {
            let actual_file = input_cpack.get_file(file_id);
            let mut actual_path = output_path.clone();
            actual_path.push(format!("{:06}.bin",file_id));
            actual_file.write_into_file(&actual_path).unwrap();
        }
        println!("done");
    } else if matches.subcommand_matches("pack").is_some() {
        let subcommand = matches.subcommand_matches("pack").unwrap();
        let input_path = PathBuf::from(subcommand.value_of("input").unwrap());
        let output_path = PathBuf::from(subcommand.value_of("output").unwrap());
        println!("creating {:#?}", output_path); //check tranformation

        // this is done to ensure files are included in the good order
        let mut max_file_nb = 0;
        for entry in fs::read_dir(&input_path).unwrap() {
            let actual_path = entry.unwrap().path();
            let file_name = actual_path.file_name().unwrap().to_string_lossy();
            let file_name_without_extension = file_name.split(".").next().unwrap();
            let file_id: u64 = file_name_without_extension.parse().unwrap();
            if file_id > max_file_nb {
                max_file_nb = file_id;
            }
        }

        let mut result_cpack: CPackCreator<Bytes> = CPackCreator::default();
        for file_id in 0..max_file_nb {
            let file_name = format!("{:06}.bin", file_id);
            let mut file_path = input_path.clone();
            file_path.push(file_name);
            let file = Bytes::new_from_path(&file_path).unwrap();
            result_cpack.push(file);
        };

        let result_file = result_cpack.write().unwrap();
        result_file.write_into_file(&output_path).unwrap();
        println!("done");
    } else {
        panic!("a subcommand was awaited!");
    }



}

fn rom_test(rom: NdsRom) {
    let known_cpack_file = [
        "data/MONSTER/m_attack.bin",
        "data/MONSTER/m_ground.bin",
        "data/MONSTER/monster.bin",
        "data/BALANCE/m_level.bin",
    ];

    println!("trying to decode/reencode cpack file");

    for cpack_path in &known_cpack_file {
        println!("trying with {}", cpack_path);
        let file = rom.read_file(PathBuf::from(cpack_path)).unwrap();
        let cpack = CPack::new_from_bytes(file).unwrap();
        let mut cpack_reencode = CPackCreator::default();
        for file_id in 0..cpack.len() {
            cpack_reencode.push(cpack.get_file(file_id))
        };
        let reencoded = cpack_reencode.write().unwrap();
        let file = rom.read_file(PathBuf::from(cpack_path)).unwrap();
        if !file.equal(&reencoded).unwrap() {
            panic!("the file is not equal")
        };
    };
    println!("finished");
}
