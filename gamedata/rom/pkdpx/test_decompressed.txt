Hello, this is just a simple test file who will be compressed with the psycommand compression tool to check if my decompressor work.
This file will be compressed with the best possible option.
(just creating nearly random file, compressing them, and uncompressing may help to spot bug, too)
ppx ppx ppx
00000000000
hhhhhhhhhhh
this is just a basic test.
Here take a few code:

/*
sir0.cpp
20/05/2014
psycommando@gmail.com

Description:

No crappyrights. All wrongs reversed !
*/
#include "sir0.hpp"

#include <types/content_type_analyser.hpp>
#include <utils/utility.hpp>
#include <utils/gbyteutils.hpp>
#include <utils/handymath.hpp>
#include <string>
#include <sstream>
#include <iomanip>
#include <array>
using namespace std;
using namespace utils;
