#[macro_use]
extern crate log;
use common::{Bytes, RABytes, get_bit};

mod tests;

#[derive(Debug)]
struct ControlFlags {
    value: Vec<u8>,
}

impl ControlFlags {
    fn new(value: Vec<u8>) -> ControlFlags {
        ControlFlags{value:value}
    }

    fn find(&self, nb_high: u8) -> Option<usize> {
        for v in 0..self.value.len() {
            if self.value[v] == nb_high {
                return Some(v);
            }
        }
        None
    }
}

/// decompress a pkdpx or at4px file. It take as input a Bytes buffer, and return a decompressed buffer (or an error)
///
/// If atomatically determine if it is a pkdpx or an at4px based on the header
/// If the file isn't the good lenght, it check if what is missing is a padding of a sir0. If it isn't, it return an error.

pub fn decompress_px(mut i: Bytes) -> Result<Bytes, &'static str> {
    debug!("decompressing a px-compressed file file");
    i.seek_0();
    let header_5 = i.read(5)?;
    if header_5 == b"PKDPX" {

        trace!("this file is a pkdpx");
        let container_lenght = i.read_u16_le()?;
        let control_flags = ControlFlags::new(i.read(9)?);
        trace!("control_flags : {:?}", control_flags);
        let decompressed_lenght = i.read_u32_le()?;
        return Ok(decompress_px_raw(i, control_flags, decompressed_lenght, container_lenght, 20)?)

    } else if header_5 == b"AT4PX" {
        //magic already checked
        let container_lenght = i.read_u16_le()?;
        let control_flags = ControlFlags::new(i.read(9)?);
        let decompressed_lenght = i.read_u16_le()? as u32;
        return Ok(decompress_px_raw(i, control_flags, decompressed_lenght, container_lenght, 18)?)
    } else {
        return Err("the magic for the PKDPX is not valid!!!");
    };

}

fn decompress_px_raw(file: Bytes, control_flags: ControlFlags, decompressed_lenght: u32, container_lenght: u16, header_lenght: u64) -> Result<Bytes, &'static str> {
    let mut result = Bytes::new();
    let mut raw_file = file.create_sub_file(file.tell(), file.len()-file.tell())?;
    trace!("starting decompression ...");
    'main: loop {
        let mut bit_num = 0;
        let byte_info = raw_file.read(1)?[0];
        trace!("command byte: 0x{:x}", byte_info);
        while bit_num < 8 {
            let this_bit = get_bit(byte_info, bit_num).unwrap();
            let this_byte = raw_file.read(1)?[0];

            match this_bit {
                true => {
                    trace!("bit is 1: pushing 0x{:2x}", this_byte);
                    result.add_a_byte(this_byte)?;
                },
                false => {
                    let nb_high: u8 = this_byte >> 4;
                    let nb_low: u8 = this_byte << 4 >> 4;
                    match control_flags.find(nb_high) {
                        Some(ctrlflagindex) => {
                            let byte_to_add = match ctrlflagindex {
                                0 => {let byte1 = (nb_low << 4) + nb_low;
                                    (byte1, byte1)},
                                _ => {
                                    let mut nybbleval = nb_low;
                                    match ctrlflagindex {
                                        1 => nybbleval += 1,
                                        5 => nybbleval -= 1,
                                        _ => (),
                                    };
                                    let mut nybbles = (nybbleval, nybbleval, nybbleval, nybbleval);
                                    match ctrlflagindex {
                                        1 => nybbles.0 -= 1,
                                        2 => nybbles.1 -= 1,
                                        3 => nybbles.2 -= 1,
                                        4 => nybbles.3 -= 1,
                                        5 => nybbles.0 += 1,
                                        6 => nybbles.1 += 1,
                                        7 => nybbles.2 += 1,
                                        8 => nybbles.3 += 1,
                                        _ => panic!(),
                                    }
                                    ((nybbles.0 << 4) + nybbles.1, (nybbles.2 << 4) + nybbles.3)
                                },
                            };
                            trace!("bit is 0: ctrlflagindex is {:x}, nb_high is {:x}, nb_low is {:x}, adding 0x{:2x}{:2x}", ctrlflagindex, nb_high, nb_low, byte_to_add.0, byte_to_add.1);
                            result.add_a_byte(byte_to_add.0)?;
                            result.add_a_byte(byte_to_add.1)?;
                        },
                        None => {
                            let new_byte = raw_file.read(1)?[0];
                            let offset_rel: i16 = -0x1000 + (((nb_low as i16)*256) + (new_byte as i16));
                            let offset = (offset_rel as i32) + (result.len() as i32);
                            let lenght = (nb_high as i32) + 3;
                            trace!("bit is 0: pushing from past, relative offset is {}, lenght is {} (nb_low:{}, nb_high:{}, new_byte:0x{:2x})", offset_rel, lenght, nb_low, nb_high, new_byte);
                            // the old, good looking code
                            result.seek(offset as u64);
                            for c in result.read(lenght as u64)? {
                                result.add_a_byte(c)?;
                            }
                        },
                    }

                },
            };
            bit_num += 1;
            if result.len() >= decompressed_lenght as u64 {
                break 'main;
            };
        }
        trace!("current output size : {}", result.len());
    }
    trace!("decoding loop finished.");
    trace!("expected container lenght: {}, read: {}", container_lenght, raw_file.tell()+20);
    trace!("expected decompressed lenght: {}, real decompressed lenght: {}", decompressed_lenght, result.len());
    if container_lenght as u64 != raw_file.tell()+header_lenght {
        return Err("the compressed lenght doesn't correspond to the real lenght of the file, assuming an error!!!");
    };

    // check if the remaining space is normal (padding from sir0)
    for _ in 0..raw_file.len()-raw_file.tell() {
        if raw_file.read(1)?[0] != 0xaa {
            return Err("the padding at the end of the file isn't a padding of sir0, assuming an error!!!");
        };
    }
    result.seek_0();
    Ok(result)
}

/// check if a file is a px-compressed filed (PKDPX or AT4PX) .
/// return true if it is one, false otherwise.
///
/// It doesn't do extensive test and don't guaranty that the file is a valid PKDPX (only check the header)
pub fn is_px(file: &Bytes) -> bool {
    if file.len() < 4 {
        return false;
    };

    let subfile = file.create_sub_file(0,5).unwrap().read(5).unwrap(); // should not panic
    if subfile == b"PKDPX" {return true};
    if subfile == b"AT4PX" {return true};
    false
}


/// use a naive compression algoritm to compress the input to a PKDPX file
pub fn naive_compression<T: RABytes>(mut file: T) -> Result<Bytes, &'static str> {
    let mut result = Bytes::new();
    // header
    result.write(b"PKDPX".to_vec())?;
    // container_lenght
    result.write_u16_le(0)?; //TODO: rewrite
    // control flags
    for _ in 0..9 {
        result.write_u8_le(0)?;
    };
    // decompressed lenght
    result.write_u32_le(file.len() as u32)?;

    let mut loop_nb = 0;
    loop {
        if loop_nb%8 == 0 {
            result.write_u8_le(0xFF)?;
        };
        result.write_u8_le(file.read_u8_le()?)?;

        if file.tell() >= file.len() {
            break;
        };
        loop_nb += 1;
    };

    let container_lenght = result.tell();
    while result.tell()%16 != 0 {
        result.write_u8_le(0xAA)?;
    }

    if container_lenght > (core::u16::MAX as u64) {
        return Err("impossible to compress the file, it is too lenghty !!!");
    };
    result.seek(5);
    result.write_u16_le(container_lenght as u16)?;

    Ok(result)
}
