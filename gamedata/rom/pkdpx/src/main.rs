use opendungeon_rom_pkdpx::{decompress_px, naive_compression};
use common::{Bytes, RABytes};
use std::path::PathBuf;
use clap::{App, SubCommand, Arg};

fn main() {
    let matches = App::new("sir0 reader")
        .arg(Arg::with_name("input")
            .short("i")
            .value_name("input")
            .required(true))
        .arg(Arg::with_name("output")
            .short("o")
            .value_name("output")
            .required(true))
        .subcommand(SubCommand::with_name("decompress")
            .about("decompress the input px-compressed file (with the appropriate header)"))
        .subcommand(SubCommand::with_name("pkdpx_compress")
            .about("compress the input to a pkdpx file")
        .arg(Arg::with_name("level")
            .short("l")
            .value_name("level")
            .required(false))
        )
        .get_matches();

    let input_file = Bytes::new_from_path(
        &PathBuf::from(matches.value_of("input").unwrap())
    ).unwrap();

    if matches.subcommand_matches("decompress").is_some() {
        println!("decompressing...");
        let decompressed = decompress_px(input_file).unwrap();
        decompressed.write_into_file(&PathBuf::from(matches.value_of("output").unwrap())).unwrap();
        println!("done");
    } else if matches.subcommand_matches("pkdpx_compress").is_some() {
        let compression_level = match matches.value_of("level") {
            Some(value) => value,
            None => "naive",
        };
        println!("compressing...");
        let compressed;
        match compression_level {
            "naive" => compressed = naive_compression(input_file).unwrap(),
            _ => panic!("there is no level compression found for the level {}", compression_level),
        };

        compressed.write_into_file(&PathBuf::from(matches.value_of("output").unwrap())).unwrap();
        println!("done");
    } else {
        panic!("a subcommand was awaited!");
    }
}
