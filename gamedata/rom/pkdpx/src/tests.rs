#[test]
fn test_pkdpx_decompression() {
    use crate::decompress_px;
    use common::{Bytes, RABytes};
    let b = Bytes::new_from_vec(include_bytes!("../test_compressed.pkdpx").to_vec());
    let decompressed = decompress_px(b).unwrap();
    assert_eq!(decompressed.read_all_vec(), include_bytes!("../test_decompressed.txt").to_vec());
}

#[test]
fn test_is_px() {
    use crate::is_px;
    use common::Bytes;

    let b = Bytes::new_from_vec(include_bytes!("../test_compressed.pkdpx").to_vec());
    assert!(is_px(&b));

    let b = Bytes::new_from_vec(b"SIR00 (this is not a pkdpx file)".to_vec());
    assert!(!is_px(&b));
}

#[test]
fn test_naive_compression() {
    use common::{Bytes, RABytes};
    use crate::naive_compression;
    use crate::decompress_px;

    let uncompressed_file = Bytes::new_from_vec(include_bytes!("../test_decompressed.txt").to_vec());
    let compressed_file = naive_compression(uncompressed_file).unwrap();

    let decompressed_file = decompress_px(compressed_file).unwrap();
    let uncompressed_file = Bytes::new_from_vec(include_bytes!("../test_decompressed.txt").to_vec());
    assert_eq!(uncompressed_file.equal(&decompressed_file).unwrap(), true);
}


//TODO: test for AT4PX
