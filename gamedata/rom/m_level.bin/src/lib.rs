//IMPORTANT: unfinished

#[macro_use]
extern crate log;
extern crate opendungeon_rom_cpack;
extern crate opendungeon_rom_pkdpx;
extern crate opendungeon_rom_sir0;
extern crate ndsrom;
extern crate common;
use opendungeon_rom_cpack::CPack;
use opendungeon_rom_sir0::Sir0;
use opendungeon_rom_pkdpx::decompress_px;
use ndsrom::NdsRom;
use std::path::PathBuf;
use common::Bytes;

/// contain the value that should be added to a monster after a level up
pub struct LevelUpData {
    /// the absolute exp required to level up
    pub exp_req: u32,
    /// the amount of HP gained
    pub hp_inc: u16,
    /// the amount of attack increase
    pub att_inc: u16,
    /// the amount of special attack increase
    pub sp_att_inc: u16,
    /// the amount of defense increase
    pub def_inc: u16,
    /// the amount of special defense increase
    pub sp_def_inc: u16,
}

/// contain the data for the level up of a monster.
pub struct MonsterLevelUpData {
    levels: Vec<LevelUpData>,
    monster_id: usize,
}

/// contain all the level up data for monsters
pub struct MLevelBin {
    monster: Vec<Option<MonsterLevelUpData>>,
}

/*impl MLevelBin {
    get_level_data(&mut self, monster_id: usize) -> Err<&MonsterLevelUpData, &'static str> {
        if monster_id >= self.monster.len() {
            return Err("tried to access a level-up data out-of-bound");
        }

    }
}*/

impl MLevelBin {
    /// create a new [`MLevelBin`] object from an ndsrom
    pub fn new_from_rom(rom: &NdsRom) -> Result<MLevelBin, &'static str> {
        Ok(MLevelBin::new_from_bytes(match rom.read_file(PathBuf::from("data/BALANCE/m_level.bin")) {
            Ok(value) => value,
            Err(_) => return Err("file data/BALANCE/m_level.bin not found/accessible on the rom"),
        }))?
    }

    /// create a new [`MLevelBin`] object from the m_level.bin file that need to be given at entrance
    pub fn new_from_bytes(b: Bytes) -> Result<MLevelBin, &'static str> {
        let mut result = MLevelBin{monster:Vec::new()};
        let cpack = CPack::new_from_bytes(b)?;
        for monster_id in 0..cpack.len() {
            result.monster.push(None);
            trace!("reading monster n°{}", monster_id);
            let mon_file = cpack.get_file(monster_id);
            //let mon_file = Sir0::new_from_bytes(mon_file)?.get_file();
            //let mon_file = decompress_px(mon_file)?;

        }
        Ok(result)
    }
}
