//extern crate clap; //TODO:
//use clap::{App, SubCommand, Arg};
extern crate ndsrom;
extern crate env_logger;
use ndsrom::NdsRom;
use opendungeon_rom_m_level_bin::MLevelBin;

pub fn main() {
    env_logger::init();

    let romfolder = "./sky-extracted";
    let rom = NdsRom::new_from_string(romfolder);
    let _m_level = MLevelBin::new_from_rom(&rom).unwrap();
}
