use common::{Bytes, RABytes};

mod tests;

pub struct Sir0 {
    file: Bytes,
    pub pointer_to_content: u32,
    pointer_offset_list: u32,
    pub offsets: Vec<u32>,
}

#[allow(clippy::cast_lossless)]
#[allow(clippy::len_without_is_empty)]
/// Important: the offset given doesn't seem to be correct (can still be used to get the file if needed)
impl Sir0 {
    pub fn new_from_bytes(file: Bytes) -> Result<Sir0, &'static str> {
        let mut result = Sir0 {
            file,
            pointer_to_content: 0,
            pointer_offset_list: 0,
            offsets: Vec::new(),
        };
        result.parse()?;
        Ok(result)
    }

    fn parse(&mut self) -> Result<(), &'static str> {
        self.offsets = Vec::new();
        self.file.seek(0); // should not fail
        if self.file.read(4)? != vec![0x53, 0x49, 0x52, 0x30] {
            return Err("The magic isn't SIR0 !!!");
        };
        self.pointer_to_content = self.file.read_u32_le()?;
        self.pointer_offset_list = self.file.read_u32_le()?;
        if self.file.read(4)? != vec![0,0,0,0] {
            return Err("The bytes that should be 0 in the header of a sir0 file aren't!!!");
        }

        self.file.seek(self.pointer_offset_list as u64);
        let mut previous_pointer = 0;
        loop {
            let mut pointer_finished = false;
            let mut actual_pointer: u32 = 0;
            let mut loop_number = 0;
            while !pointer_finished {
                let this_pointer = self.file.read(1)?[0];
                let is_continuing = match this_pointer >> 7 {
                    0 => false,
                    1 => true,
                    _ => return Err("u8 >> 7 should return either 0 or 1 (error in sir0)!!!"),
                };
                actual_pointer += (((this_pointer << 1) >> 1) as u32) << (loop_number*7);
                if !is_continuing {
                    pointer_finished = true;
                }
                loop_number += 1;
                if loop_number >= 4 {
                    return Err("integer overflow in the parsing of pointer of sir0!!!");
                }
            }
            if actual_pointer == 0 {
                break;
            };
            actual_pointer += previous_pointer;
            self.offsets.push(actual_pointer);
            previous_pointer = actual_pointer;
        }
        if self.offsets.len() < 2 {
            return Err("the internal pointer are not present in the offset table of the sir0!!!");
        };
        Ok(())
    }

    /// return the pointer x, including the two first 4 and 8 pointer, and relative to the input file start
    pub fn get_pointer_raw(&self, pointer_id: u32) -> u32 {
        self.offsets[pointer_id as usize]
    }

    /// return the pointer x, including internal value, and relative to the beggining to self.get_file()
    pub fn get_pointer(&self, pointer_id: u32) -> u32 {
        self.get_pointer_raw(pointer_id+2)-self.pointer_to_content
    }

    /// return the file contained in the sir0 (padding at the end not deleted)
    pub fn get_file(&self) -> Bytes {
        self.file.create_sub_file(u64::from(self.pointer_to_content),
            u64::from(self.pointer_offset_list-self.pointer_to_content)
        ).unwrap()
    }

    /// return the number of offset (without internal value)
    pub fn len(&self) -> usize {
        self.offsets.len()-2
    }

    /// return the number of offset including internal ones
    pub fn len_raw(&self) -> usize {
        self.offsets.len()
    }

    /// write an offset list based of the input offsets
    pub fn write_offset_list<B: RABytes>(file: &mut B, offsets: &[u64]) -> Result<(), &'static str> {
        let mut last_written_offset = 0;
        for offset in offsets {
            let mut offset_to_write = *offset-last_written_offset;
            let mut inversed_value: Vec<u8> = vec![]; // the output for this offset, but in the inversed order
            let mut repetition = 0;
            while offset_to_write != 0 {
                let mut actual_byte = (offset_to_write & 0x7F) as u8;
                offset_to_write >>= 7;
                if repetition >= 1 {
                    actual_byte += 128;
                };
                inversed_value.push(actual_byte);
                repetition += 1;
            };
            for id in 0..inversed_value.len() {
                file.write_u8_le(inversed_value[inversed_value.len()-1-id])?;
            };
            last_written_offset = *offset;
        };
        file.write_u8_le(0)?;

        Ok(())
    }
}
