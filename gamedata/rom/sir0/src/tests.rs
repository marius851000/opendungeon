
#[test]
fn test_sir0_read() {
    use crate::Sir0;
    use common::{Bytes, RABytes};
    let buf = Bytes::new_from_vec(
        vec![
            // header-16B
            0x53,0x49,0x52,0x30, //0-b"SIR0"
            16,0,0,0, //4-pointer to data
            32,0,0,0, //8-pointer to offset list
            0,0,0,0, //12-0s
            // file content-varies (16B here)
            0,1,2,3,4,5,6,7,8,9,10,11,12,13,14,15, //16-content
            // pointer TOC
            4,4,16,0, //32-TOC
            0xaa,0xaa,0xaa,0xaa,0xaa,0xaa,0xaa,0xaa,0xaa,0xaa,0xaa,0xaa, // padding
        ],
    );
    let sir0 = Sir0::new_from_bytes(buf).unwrap();
    assert_eq!(sir0.get_pointer(0), 8);
    let mut file = sir0.get_file();
    file.seek(sir0.get_pointer(0) as u64);
    assert_eq!(file.read(1).unwrap()[0], 8);
}
