use opendungeon_rom_sir0::Sir0;
use common::{Bytes};
use std::path::PathBuf;
use clap::{App, SubCommand, Arg};

fn main() {
    let matches = App::new("sir0 reader")
        .arg(Arg::with_name("input")
            .short("i")
            .value_name("input")
            .required(true))
        .subcommand(SubCommand::with_name("displaypointer")
            .about("display all the (raw) pointer in the file"))
        .get_matches();

    let input_file = Bytes::new_from_path(
        &PathBuf::from(matches.value_of("input").unwrap())
    ).unwrap();

    if matches.subcommand_matches("displaypointer").is_some() {
        let sir0 = Sir0::new_from_bytes(input_file).unwrap();
        for l in 0..sir0.len() as u32 {
            println!("{}: {}", l, sir0.get_pointer_raw(l));
        };
    } else {
        panic!("a subcommand was awaited!");
    }
}
