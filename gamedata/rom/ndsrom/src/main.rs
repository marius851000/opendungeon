extern crate clap;
use clap::{App, SubCommand, Arg};
use ndsrom::NdsRom;
use std::path::PathBuf;
use common::RABytes;

pub fn main() {
    let matches = App::new("ndsrom test")
        .arg(Arg::with_name("romfolder")
            .short("r")
            .value_name("rom_folder")
            .required(true))
        .subcommand(SubCommand::with_name("display")
            .about("display a raw file of the rom")
            .arg(Arg::with_name("file")
                .short("f")
                .value_name("file")
                .required(true)))
        .get_matches();

    let rom = NdsRom::new_from_string(matches.value_of("romfolder").unwrap());

    if let Some(matches) = matches.subcommand_matches("display") {
        let mut file = match rom.read_file(PathBuf::from(matches.value_of("file").unwrap())) {
            Ok(f) => f,
            Err(e) => panic!("error while reading file: {:?}!", e),
        };
        println!("{:?}", file.read(file.len()));
    } else {
        panic!("a subcommand was awaited!")
    }
}
