use std::path::PathBuf;
use ::common::Bytes;

/// A struct that represent a nintendo ds rom, and a way to access it, as decompressed by ndstool
pub struct NdsRom {
    /// the root path to the decompressed folder by ndstool. Should contain data, arm7.bin, and other file.
    rom_path: PathBuf,
}

impl NdsRom {
    pub fn new(rom_path: PathBuf) -> NdsRom {
        NdsRom{rom_path: rom_path}
    }
    pub fn new_from_string(rom_path: &str) -> NdsRom {
        NdsRom::new(PathBuf::from(rom_path))
    }
    pub fn read_file(&self, file_path: PathBuf) -> std::io::Result<Bytes> {
        let mut absolute_path = self.rom_path.clone();
        absolute_path.push(file_path);
        Bytes::new_from_path(&absolute_path)
    }
}
