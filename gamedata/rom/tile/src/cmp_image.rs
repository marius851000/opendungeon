use opendungeon_common::{Bytes, RABytes};

enum CmpCommand {
    CyclePatternAndCopy,
    CyclePatternAndCopyNbCopyNextByte,

    UseLastPatternAndCopy,
    UseLastPatternAndCopyNbCopyNextByte,

    LoadByteAsPatternAndCopy,
    LoadByteAsPatternAndCopyNbCopyNextByte,

    LoadNextByteAsNumberToCopy,
    LoadNextWordAsNumberToCopy,

    Other(u8),
}

impl CmpCommand {
    fn get_command(command_byte: u8) -> CmpCommand {
        match command_byte {
            0xE0 => CmpCommand::CyclePatternAndCopy,
            0xFF => CmpCommand::CyclePatternAndCopyNbCopyNextByte,

            0xC0 => CmpCommand::UseLastPatternAndCopy,
            0xDF => CmpCommand::UseLastPatternAndCopyNbCopyNextByte,

            0x80 => CmpCommand::LoadByteAsPatternAndCopy,
            0xBF => CmpCommand::LoadByteAsPatternAndCopyNbCopyNextByte,

            0x7E => CmpCommand::LoadNextByteAsNumberToCopy,
            0x7F => CmpCommand::LoadNextWordAsNumberToCopy,

            x => CmpCommand::Other(x),
        }
    }
    fn get_byte(self) -> u8 {
        match self {
            CmpCommand::CyclePatternAndCopy => 0xEF,
            CmpCommand::CyclePatternAndCopyNbCopyNextByte => 0xFF,
            CmpCommand::UseLastPatternAndCopy => 0xC0,
            CmpCommand::UseLastPatternAndCopyNbCopyNextByte => 0xDF,
            CmpCommand::LoadByteAsPatternAndCopy => 0x80,
            CmpCommand::LoadByteAsPatternAndCopyNbCopyNextByte => 0xBF,
            CmpCommand::LoadNextByteAsNumberToCopy => 0x7E,
            CmpCommand::LoadNextWordAsNumberToCopy => 0x7F,
            CmpCommand::Other(x) => x,
        }
    }
}

pub struct CmpImage { //compression seem to be the PX one

}

impl CmpImage {
    pub fn new_from_bytes(mut file: &mut Bytes, nb_tiles: u16) -> Result<CmpImage, &'static str>{
        let mut parsed_bytes = 0;
        loop {
            let command = CmpCommand::get_command(file.read_u8_le()?);
            let number_word_out = determine_lenght_to_output(command, &mut file)?;
        }
        Ok(CmpImage{})
    }
}

fn determine_lenght_to_output(command: CmpCommand, file: &mut Bytes) -> Result<u64, &'static str> {
    let mut number_bytes_to_copy: u64;
    match &command {
        CmpCommand::CyclePatternAndCopyNbCopyNextByte |
        CmpCommand::LoadByteAsPatternAndCopyNbCopyNextByte |
        CmpCommand::LoadByteAsPatternAndCopyNbCopyNextByte |
        CmpCommand::LoadNextByteAsNumberToCopy => number_bytes_to_copy = file.read_u8_le()? as u64,
        CmpCommand::LoadNextWordAsNumberToCopy => {
            println!("not sure if that is a le or be!!! Remember to check this!!!");
            number_bytes_to_copy = file.read_u16_le()? as u64;
        },
        other => {
            number_bytes_to_copy = other.get_byte() as u64;
            if other.get_byte() >= CmpCommand::CyclePatternAndCopy.get_byte() {
                number_bytes_to_copy -= CmpCommand::CyclePatternAndCopy.get_byte() as u64;
            } else if other.get_byte() >= CmpCommand::UseLastPatternAndCopy.get_byte() {
                number_bytes_to_copy -= CmpCommand::UseLastPatternAndCopy.get_byte() as u64;
            } else if other.get_byte() >= CmpCommand::LoadByteAsPatternAndCopy.get_byte() {
                number_bytes_to_copy -= CmpCommand::LoadByteAsPatternAndCopy.get_byte() as u64
            }
        }
    }
    Ok(number_bytes_to_copy)
}
