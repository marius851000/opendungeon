use opendungeon_common::{Bytes, RABytes};
mod cmp_image;
use cmp_image::CmpImage;

struct TilesHeader {
    pointer_up_layer: Option<u16>,
    pointer_down_layer: Option<u16>,
}

impl TilesHeader {
    fn new_from_bytes(file: &mut Bytes) -> Result<TilesHeader, &'static str> {
        let pointer_up_layer = file.read_u16_le()?;
        let pointer_down_layer = file.read_u16_le()?;
        Ok(TilesHeader {
            pointer_up_layer: match pointer_up_layer {0=>None, x=>Some(x)},
            pointer_down_layer: match pointer_down_layer {0=>None, x=>Some(x)},
        })
    }
}

struct SizeTable {
    nb_tiles: u16,
}

impl SizeTable {
    fn new_from_bytes(file: &mut Bytes) -> Result<SizeTable, &'static str> {
        let nb_tiles = file.read_u16_le()?;
        Ok(SizeTable {
            nb_tiles: nb_tiles,
        })
    }
}

struct LayerData {
    cmp_image: CmpImage,
}

impl LayerData {
    fn new_from_bytes(mut file: &mut Bytes, size_table: &SizeTable) -> Result<LayerData, &'static str> {
        let cmp_image = CmpImage::new_from_bytes(&mut file, size_table.nb_tiles)?;
        Ok(LayerData{
            cmp_image: cmp_image,
        })
    }
}

struct TilesTemp {
    header: Option<TilesHeader>,
    size_table: (Option<SizeTable>, Option<SizeTable>),
}

impl TilesTemp {
    fn new_empty() -> TilesTemp {
        TilesTemp {
            header: None,
            size_table: (None,None),
        }
    }

    fn parse_header(&mut self, mut file: &mut Bytes) -> Result<(), &'static str> {
        self.header = Some(TilesHeader::new_from_bytes(&mut file)?);
        Ok(())
    }

    fn parse_size_table(&mut self, mut file: &mut Bytes) -> Result<(), &'static  str> {
        let header: &TilesHeader;
        match &self.header {
            Some(rheader) => header = rheader,
            None => return Err("impossible to get the header in parse_size_table"),
        };
        if header.pointer_up_layer.is_some() {
            self.size_table.0 = Some(SizeTable::new_from_bytes(&mut file)?);
        };
        if header.pointer_down_layer.is_some() {
            self.size_table.1 = Some(SizeTable::new_from_bytes(&mut file)?);
        };
        Ok(())
    }

    fn parse_layers_data(&mut self, mut file: &mut Bytes) -> Result<(), &'static str> {
        let header: &TilesHeader;
        match &self.header {
            Some(rheader) => header = rheader,
            None => return Err("impossible to get the header in parse_layers_data"),
        };
        if header.pointer_up_layer.is_some() {
            let size_table_up = match &self.size_table.0 {Some(x)=>x,None=>return Err("impossible to get the data for the upper layer")};
            let upper_layer_data = LayerData::new_from_bytes(&mut file, size_table_up)?;
            //TODO:
        };
        if header.pointer_down_layer.is_some() {
            println!("parsing downer layer");
            panic!("unimplemented");
        };
        Ok(())
    }
}

pub struct Tiles {

}

impl Tiles {
    pub fn new_from_bytes(mut file: &mut Bytes) -> Result<(), &'static str>{
        let mut tiles_temp = TilesTemp::new_empty();
        tiles_temp.parse_header(&mut file)?;
        tiles_temp.parse_size_table(&mut file)?;
        tiles_temp.parse_layers_data(&mut file)?;
        Ok(())
    }
}
