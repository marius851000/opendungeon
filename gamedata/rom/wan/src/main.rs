#![recursion_limit = "1024"]
#[macro_use]
extern crate error_chain;
extern crate env_logger;

use std::path::PathBuf;

use opendungeon_common::{Bytes, RABytes};
use opendungeon_rom_wan::errors::*;
use opendungeon_rom_wan::WanImage;
use clap::{App, SubCommand, Arg};

quick_main!(run);

fn run() -> Result<()> {
    env_logger::init();

    let matches = App::new("opendungeon wan editor")
        .arg(Arg::with_name("input")
            .short("i")
            .value_name("input")
            .required(true))
        .arg(Arg::with_name("output")
            .short("o")
            .value_name("output")
            .required(true))
        .subcommand(SubCommand::with_name("reencode")
            .about("parse and reencode the sprite (uncompressed)"))
        .get_matches();

    if matches.subcommand_matches("reencode").is_some() {
        let input_path = PathBuf::from(matches.value_of("input").unwrap());
        let output_path = PathBuf::from(matches.value_of("output").unwrap());
        let input_file = Bytes::new_from_path(&input_path).unwrap();
        let mut decoded_sprite = WanImage::new_from_bytes(input_file).unwrap();
        let reencoded_sprite = WanImage::create_wan(&decoded_sprite).unwrap();
        reencoded_sprite.write_into_file(&output_path).unwrap();
        Ok(())
    } else {
        panic!("a subcommand was awaited!");
    }
}
