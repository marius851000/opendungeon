extern crate opendungeon_common;
#[macro_use]
extern crate log;

#[macro_use]
extern crate error_chain;

pub mod errors {
    error_chain!{
    }
}

pub mod wan;
pub use wan::WanImage;
