extern crate image;
use std::collections::HashMap;
use opendungeon_common::Bytes;
use opendungeon_common::RABytes;
use opendungeon_common::get_bit_u16;
use opendungeon_rom_sir0::Sir0;
use super::errors::*;


pub enum SpriteType {
    PropsUI,
    Chara,
    Unknown,
}

impl SpriteType {
    fn get_id(&self) -> u8 {
        match self {
            SpriteType::PropsUI => 0,
            SpriteType::Chara => 1,
            SpriteType::Unknown => 3,
        }
    }
}

#[derive(Debug)]
pub struct MetaFrame {
    unk1: u16,
    unk2: u16,
    unk3: bool,
    pub image_index: usize,
    pub offset_y: i32,
    pub offset_x: i32,
    is_last: bool,
    pub v_flip: bool,
    pub h_flip: bool,
    pub is_mosaic: bool,
    resolution: Option<Resolution::<u8>>,
}

#[derive(Debug, Copy, Clone)]
struct Resolution<T> {
    x: T,
    y: T,
}

impl Resolution<u8> {
    pub fn get_indice(self) -> (u16, u16) {
        match (self.y, self.x) {
            (8,8) => (0,0),
            (16,16) => (0,1),
            (32,32) => (0,2),
            (64,64) => (0,3),
            (16,8) => (1,0),
            (8,16) => (2,0),
            (32,8) => (1,1),
            (8,32) => (2,1),
            (32,16) => (1,2),
            (16,32) => (2,2),
            (64,32) => (1,3),
            (32,64) => (2,3),
            _ => panic!(),
        }
    }
}

impl MetaFrame {
    fn new_from_bytes(file: &mut Bytes, previous_image: Option<usize>) -> Result<MetaFrame> {
        trace!("parsing a meta-frame");
        let image_index = match file.read_i16_le()? {
            -1 => match previous_image {
                None => bail!("an image id point to the same than the previous one, but it is the first image"),
                Some(value) => value,
            },
            x => {if x >= 0 {
                x as usize
            } else {
                bail!("a metaframe is less than -1");
            }},
        };

        let unk1 = file.read_u16_le()?;

        // they are quite strangely encoded (the fact I should read as little-endian the 2 byte correctly reading them)

        // bit in ppmdu tool are right to left !!!
        let offset_y_data = file.read_u16_le()?;
        let size_indice_y = ((0xC000 & offset_y_data) >> (8+6)) as u8;
        let is_mosaic = get_bit_u16(offset_y_data, 3).unwrap();
        let unk3 = get_bit_u16(offset_y_data, 7).unwrap();
        let offset_y = i16::from_le_bytes((offset_y_data & 0x00FF).to_le_bytes()) as i32; //range: 0-255

        let offset_x_data = file.read_u16_le()?;
        let size_indice_x = ((0xC000 & offset_x_data) >> (8+6)) as u8;
        let v_flip = get_bit_u16(offset_x_data, 2).unwrap();
        let h_flip = get_bit_u16(offset_x_data, 3).unwrap();
        let is_last = get_bit_u16(offset_x_data, 4).unwrap();
        let offset_x = (i16::from_le_bytes((offset_x_data & 0x01FF).to_le_bytes()) as i32) - 256; //range: 0-511

        let unk2 = file.read_u16_le()?;

        Ok(MetaFrame {
            unk1, unk2, unk3,
            image_index: image_index,
            offset_x: offset_x,
            offset_y: if offset_y > 128 {
                offset_y-256
            } else {
                offset_y
            },
            is_last: is_last,
            v_flip: v_flip,
            h_flip: h_flip,
            is_mosaic: is_mosaic,
            resolution: match (size_indice_y << 4) + size_indice_x {
                0x00 => Some(Resolution{x:8,y:8}),
                0x01 => Some(Resolution{x:16,y:16}),
                0x02 => Some(Resolution{x:32,y:32}),
                0x03 => Some(Resolution{x:64,y:64}),
                0x10 => Some(Resolution{x:16,y:8}),
                0x20 => Some(Resolution{x:8,y:16}),
                0x11 => Some(Resolution{x:32,y:8}),
                0x21 => Some(Resolution{x:8,y:32}),
                0x12 => Some(Resolution{x:32,y:16}),
                0x22 => Some(Resolution{x:16,y:32}),
                0x13 => Some(Resolution{x:64,y:32}),
                0x23 => Some(Resolution{x:32,y:64}),
                _ => None, // seem to be normal
            }
        })
    }
    fn is_last(&self) -> bool {
        self.is_last
    }
    fn write(file: &mut Bytes, meta_frame: &MetaFrame, previous_image: Option<usize>) -> Result<()> {
        let image_index: i16 = match previous_image {
            None => meta_frame.image_index as i16,
            Some(value) => if meta_frame.image_index == value {
                -1
            } else {
                meta_frame.image_index as i16
            },
        };
        file.write_i16_le(image_index).unwrap();

        file.write_u16_le(meta_frame.unk1).unwrap(); //unk
        let (size_indice_y, size_indice_x) = match meta_frame.resolution {
            Some(value) => value.get_indice(),
            None => panic!(),
        };

        let offset_y_data: u16 = (size_indice_y << 13) +
            if meta_frame.is_mosaic {
                (1 << 12)
            } else {0} +
            ((meta_frame.unk3 as u16) << (16-7-1)) +
            (u16::from_le_bytes((meta_frame.offset_y as i16).to_le_bytes()) & 0x00FF);
        file.write_u16_le(offset_y_data).unwrap();

        let offset_x_data: u16 = (size_indice_x << 14) +
            ((meta_frame.v_flip as u16) << (16-2-1)) +
            ((meta_frame.h_flip as u16) << (16-3-1)) +
            ((meta_frame.is_last as u16) << (16-4-1)) +
            (u16::from_le_bytes(((meta_frame.offset_x + 256) as i16).to_le_bytes()) & 0x01FF);
        file.write_u16_le(offset_x_data).unwrap();

        file.write_u16_le(meta_frame.unk2).unwrap();

        Ok(())
    }
}

pub struct MetaFrameGroup {
    pub meta_frames_id: Vec<usize>,
}

impl MetaFrameGroup {
    fn new_from_bytes(mut file: &mut Bytes, meta_frames: &mut Vec<MetaFrame>) -> Result<MetaFrameGroup> {
        let mut meta_frames_id = Vec::new();
        let mut previous_image = None;
        loop {
            meta_frames_id.push(meta_frames.len()); // We refer to the metaframe we will put here
            let meta_frame = MetaFrame::new_from_bytes(&mut file, previous_image)?;
            previous_image = Some(meta_frame.image_index);
            meta_frames.push(meta_frame);
            trace!("it's data: {:?}", meta_frames[meta_frames.len()-1]);
            if meta_frames[meta_frames.len()-1].is_last() {
                break
            }
        }
        Ok(MetaFrameGroup {
            meta_frames_id: meta_frames_id,
        })
    }

    fn write(mut file: &mut Bytes, meta_frame_group: &MetaFrameGroup, meta_frames: &Vec<MetaFrame>) -> Result<()> {
        let mut previous_image: Option<usize> = None; //for what purpose ?
        for l in 0..meta_frame_group.meta_frames_id.len() {
            let meta_frames_id = meta_frame_group.meta_frames_id[l];
            let meta_frame_to_write = &meta_frames[meta_frames_id];
            MetaFrame::write(&mut file, meta_frame_to_write, previous_image).unwrap();
            previous_image = Some(l);
        }
        Ok(())
    }
}
pub struct MetaFrameStore {
    pub meta_frames: Vec<MetaFrame>,
    pub meta_frame_groups: Vec<MetaFrameGroup>
}

impl MetaFrameStore {
    // assume that the pointer is already well positionned
    fn new_from_bytes(mut file: &mut Bytes, nb_meta_frame: u64) -> Result<MetaFrameStore> {
        let mut meta_frames = Vec::new();
        let mut meta_frame_groups = Vec::new();
        let mut last_pointer = None;


        let mut meta_frame_reference: Vec<u64> = Vec::new();
        for _ in 0..nb_meta_frame {
            let actual_ptr = file.read_u32_le()? as u64;
            //some check
            match last_pointer {
                None => last_pointer = Some(actual_ptr),
                Some(value) => if (actual_ptr-value)%10 != 0 {
                    bail!("in the creation of a meta frame store: the check for the offset of the pointer of the animation group are not valid!");
                }
            };
            meta_frame_reference.push(actual_ptr);
        }

        for meta_frame_id in 0..nb_meta_frame {
            trace!("parsing meta-frame n°{} (at offset {})", meta_frame_id, meta_frame_reference[meta_frame_id as usize]);
            file.seek(meta_frame_reference[meta_frame_id as usize]);
            meta_frame_groups.push(MetaFrameGroup::new_from_bytes(&mut file, &mut meta_frames)?);
        }
        Ok(MetaFrameStore {
            meta_frames: meta_frames,
            meta_frame_groups: meta_frame_groups,
        })
    }

    fn find_resolution_image(&self, image_id: u32) -> Result<Option<Resolution::<u8>>> {
        for actual_image in &self.meta_frames {
            if actual_image.image_index == image_id as usize {
                return Ok(actual_image.resolution);
            };
        }
        bail!("the resolution was not found!!!");
    }

    fn write(mut file: &mut Bytes, meta_frame_store: &MetaFrameStore) -> Result<Vec<u32>> {
        let nb_meta_frame = meta_frame_store.meta_frame_groups.len();
        let mut meta_frame_references = vec![];

        for l in 0..nb_meta_frame {
            meta_frame_references.push(file.tell() as u32);
            MetaFrameGroup::write(&mut file, &meta_frame_store.meta_frame_groups[l], &meta_frame_store.meta_frames).unwrap();
        };


        Ok(meta_frame_references)
    }
}

#[derive(Debug)]
pub struct ImageAssemblyEntry {
    pixel_src: u64,
    pixel_amount: u64,
    byte_amount: u64,
    _z_index: u32,
}

impl ImageAssemblyEntry {
    fn new_from_bytes(file: &mut Bytes) -> Result<ImageAssemblyEntry> {
        let pixel_src = file.read_u32_le()? as u64;
        let byte_amount = file.read_u16_le()? as u64;
        let pixel_amount = byte_amount * 2;
        file.read(2)?;
        let z_index = file.read_u32_le()?;
        Ok(ImageAssemblyEntry{
            pixel_src: pixel_src,
            pixel_amount: pixel_amount,
            byte_amount: byte_amount,
            _z_index: z_index,
        })
    }
    fn is_null(&self) -> bool {
        if self.pixel_amount == 0 && self.pixel_src == 0 {
            true
        } else {
            false
        }
    }
    fn write(&self, file: &mut Bytes) -> Result<()> {
        file.write_u32_le(self.pixel_src as u32).unwrap();
        file.write_u16_le(self.byte_amount as u16).unwrap();
        file.write_u16_le(0).unwrap();
        file.write_u32_le(self._z_index).unwrap();
        Ok(())
    }
}

/// an helper struct, that permit to know where to place the next pixel
struct ImgPixelPointer {
    xsize: u32,
    //ysize: u32,
    true_column: u32,
    true_line: u32,
    line: u32,
    column: u32,
}

impl ImgPixelPointer {
    fn new(resx: u32, _resy: u32) -> ImgPixelPointer {
        ImgPixelPointer {
            xsize: resx,
            //ysize: resy,
            true_column: 0,
            true_line: 0,
            line: 0,
            column: 0,
        }
    }

    fn next(&mut self) -> Resolution::<u32> {
        let tile_width = 8;
        let tile_height = 8;

        let mut x = self.true_column*8+self.column;
        match self.column % 2 {
            0 => x+=1,
            1 => x-=1,
            _ => panic!(),
        };
        let y = self.true_line*8+self.line;
        self.column += 1;
        if self.column >= tile_width {
            self.column=0;
            self.line+=1;
            if self.line >= tile_height {
                self.line=0;
                self.true_column+=1;
                if self.true_column >= self.xsize/tile_width {
                    self.true_column=0;
                    self.true_line+=1;
                }
            }
        }
        Resolution::<u32> {
            x:x,
            y:y,
        }
    }
}

pub struct Image {
    pub img: image::ImageBuffer<image::Rgba<u8>, Vec<u8>>,
    pub z_index: u32,
}

impl Image {
    fn new_from_bytes(mut file: &mut Bytes, resolution: Resolution::<u8>, palette: &Palette) -> Result<Image> {
        let mut img_asm_table = Vec::new();
        let mut image_size = 0;

        let mut last_pointer = None; //for check
        loop {
            let asm_entry = ImageAssemblyEntry::new_from_bytes(&mut file)?;
            image_size += asm_entry.pixel_amount;
            if asm_entry.is_null() {
                break
            } else {
                trace!("part amount: {}, point to: {}", asm_entry.pixel_amount, asm_entry.pixel_src);
                if asm_entry.pixel_src != 0 {
                    match last_pointer {
                        None => last_pointer = Some(asm_entry.pixel_src + asm_entry.byte_amount),
                        Some(value) => if value == asm_entry.pixel_src {
                            last_pointer = Some(asm_entry.byte_amount + value);
                        } else {
                            bail!("pointer to image parts are not coherent.");
                        }
                    }
                };
                img_asm_table.push(asm_entry);
            }
        }
        trace!("the resolution of this image is ({}, {})", resolution.x, resolution.y);
        trace!("the image contain {} assembly entry, with an image size of {}.", img_asm_table.len(), image_size);

        // transform to image on the fly
        let mut img = image::ImageBuffer::new(resolution.x as u32, resolution.y as u32);

        let mut img_pixel_pointer = ImgPixelPointer::new(resolution.x as u32, resolution.y as u32);
        let mut z_index = None;
        for entry in &img_asm_table {
            if entry.pixel_src == 0 {
                for _ in 0..entry.pixel_amount {
                    let pixel_pos = img_pixel_pointer.next();
                    let pixel = img.get_pixel_mut(pixel_pos.x, pixel_pos.y);
                    *pixel = image::Rgba([0,0,0,0]);
                }
            } else {
                file.seek(entry.pixel_src);
                for _ in 0..entry.pixel_amount {
                    let color_id = file.read_u4_le()?;
                    let pixel_pos = img_pixel_pointer.next();
                    let pixel = img.get_pixel_mut(pixel_pos.x, pixel_pos.y);
                    if color_id == 0 {
                        *pixel = image::Rgba([0,0,0,0]);
                    } else {
                        let color = palette.get(color_id as usize)?;
                        let color = [color.0, color.1, color.2, 255];
                        *pixel = image::Rgba(color);
                    };
                }
            }
            match z_index {
                None => z_index = Some(entry._z_index),
                Some(index) => {
                    debug_assert!(index == entry._z_index);
                    z_index = Some(entry._z_index);
                }
            }
        }

        let z_index = match z_index {
            Some(value) => value,
            None => bail!("impossible to find a definied z_index (the image is probably empty)!!! aborting"),
        };

        Ok(Image{
            img,
            z_index,
        })
    }

    fn write(&self, mut file: &mut Bytes, palette: &Palette) -> Result<(u64, Vec<u64>)> {
        let resolution = (self.img.width(), self.img.height());
        // generate the pixel list
        let group_resolution = (self.img.width()/8, self.img.height()/8);

        let mut pixel_list: Vec<u8> = vec![]; //a value = a pixel (acording to the tileset). None is fully transparent

        for y_group in 0..group_resolution.1 {
            for x_group in 0..group_resolution.0 {
                    for in_group_y in 0..8 {
                        for in_group_x in vec![1,0,3,2,5,4,7,6] {
                        let real_x_pixel = x_group*8+in_group_x;
                        let real_y_pixel = y_group*8+in_group_y;
                        let real_color = self.img.get_pixel(real_x_pixel, real_y_pixel);
                        let real_color_tuple = (real_color[0], real_color[1], real_color[2], match real_color[3] {
                            255 => 128,
                            0 => 0,
                            _ => bail!("an impossible alpha level was found in the picture !"),
                        });
                        pixel_list.push(if real_color_tuple == (0,0,0,0) {
                            0
                        } else {
                            palette.color_id(real_color_tuple).unwrap() as u8
                        });
                    };
                };
            };
        };
        // those value allow to have the same output of the original file
        //TODO: make the value change if we don't need to have the same output
        let min_transparent_to_compress = 32; //TODO: take care of the palette len
        let multiple_of_value = 2;
        let use_legacy_compression = true; //set to false to use a better compression algo

        let mut assembly_table: Vec<ImageAssemblyEntry> = vec![];

        if !use_legacy_compression {
            let mut number_of_byte_to_include = 0;
            let mut byte_include_start = file.tell();

            let mut pixel_id = 0;
            loop {
                debug_assert!(pixel_id%2 == 0);
                let mut should_create_new_transparent_entry = false;

                if (pixel_id%multiple_of_value == 0) && (pixel_id + min_transparent_to_compress < pixel_list.len()) {
                    let mut encontered_non_transparent = false;
                    for l in 0..min_transparent_to_compress {
                        if pixel_list[pixel_id+l] != 0 {
                            encontered_non_transparent = true;
                            break;
                        };
                    };
                    if !encontered_non_transparent {
                        should_create_new_transparent_entry = true;
                    };
                };

                if should_create_new_transparent_entry {
                    //push the actual content
                    if number_of_byte_to_include > 0 {
                        assembly_table.push(ImageAssemblyEntry {
                            pixel_src: byte_include_start,
                            pixel_amount: number_of_byte_to_include * 2,
                            byte_amount: number_of_byte_to_include,
                            _z_index: self.z_index,
                        });
                        number_of_byte_to_include = 0;
                        byte_include_start = file.tell();
                    };
                    //create new entry for transparent stuff
                    //count the number of transparent tile
                    let mut transparent_tile_nb = 0;
                    loop {
                        if pixel_id >= pixel_list.len() {
                            break
                        };
                        if pixel_list[pixel_id] == 0 {
                            transparent_tile_nb += 1;
                            pixel_id += 1;
                        } else {
                            break
                        };
                    };
                    if pixel_id%multiple_of_value != 0 {
                        transparent_tile_nb -= pixel_id%multiple_of_value;
                        pixel_id -= pixel_id%multiple_of_value;
                    };
                    assembly_table.push(ImageAssemblyEntry {
                        pixel_src: 0,
                        pixel_amount: transparent_tile_nb as u64,
                        byte_amount: (transparent_tile_nb as u64)/2, //TODO: take care of the tileset lenght
                        _z_index: self.z_index,
                    });

                    continue;
                };

                if pixel_id >= pixel_list.len() {
                    break
                };
                debug_assert!(pixel_list[pixel_id] < 16);
                debug_assert!(pixel_list[pixel_id+1] < 16);
                file.write_u8_le((pixel_list[pixel_id] << 4) + pixel_list[pixel_id+1]).unwrap();
                pixel_id += 2;
                number_of_byte_to_include += 1;
            };
            if number_of_byte_to_include > 0 {
                assembly_table.push(ImageAssemblyEntry {
                    pixel_src: byte_include_start,
                    pixel_amount: number_of_byte_to_include * 2,
                    byte_amount: number_of_byte_to_include,
                    _z_index: self.z_index,
                });
                number_of_byte_to_include = 0;
                byte_include_start = file.tell();
            };
        } else {
            enum ActualEntry {
                Null(u64, u32), //lenght (pixel), z_index
                Some(u64, u64, u32), // initial_offset, lenght (pixel), z_index
            }

            impl ActualEntry {
                fn new(is_all_black: bool, start_offset: u64, z_index: u32) -> ActualEntry {
                    if is_all_black {
                        ActualEntry::Null(64, z_index)
                    } else {
                        ActualEntry::Some(start_offset, 64, z_index)
                    }
                }

                fn to_assembly(&self) -> ImageAssemblyEntry {
                    match self {
                        ActualEntry::Null(lenght, z_index) => ImageAssemblyEntry {
                            pixel_src: 0,
                            pixel_amount: *lenght,
                            byte_amount: *lenght/2,
                            _z_index: *z_index,
                        },
                        ActualEntry::Some(initial_offset, lenght, z_index) => ImageAssemblyEntry {
                            pixel_src: *initial_offset,
                            pixel_amount: *lenght,
                            byte_amount: *lenght/2,
                            _z_index: *z_index,
                        }
                    }
                }

                fn advance(&self, lenght: u64) -> ActualEntry{
                    match self {
                        ActualEntry::Null(l, z) => ActualEntry::Null(*l + lenght, *z),
                        ActualEntry::Some(offset, l, z) => ActualEntry::Some(*offset, *l+lenght, *z),
                    }
                }
            }

            let mut actual_entry: Option<ActualEntry> = None;

            for loop_nb in 0..(group_resolution.0 * group_resolution.1) {
                let mut this_area = vec![];
                let mut is_all_black = true;
                for l in 0..64 {
                    let actual_pixel = pixel_list[(loop_nb*64+l) as usize];
                    this_area.push(actual_pixel);
                    if actual_pixel != 0 {
                        is_all_black = false;
                    };
                };

                let pos_before_area = file.tell();
                if !is_all_black {
                    for byte_id in 0..32 {
                        file.write_u8_le((this_area[byte_id*2] << 4) + this_area[byte_id*2+1]).unwrap();
                    }
                }

                let need_to_create_new_entry = if actual_entry.is_none() {
                    true
                } else {
                    match &actual_entry {
                        Some(ActualEntry::Null(_,_)) => if is_all_black {false} else {true},
                        Some(ActualEntry::Some(_,_,_)) => if is_all_black {true} else {false},
                        _ => panic!(),
                    }
                };

                if need_to_create_new_entry {
                    match actual_entry {
                        Some(entry) => assembly_table.push(entry.to_assembly()),
                        None => (),
                    }
                    actual_entry = Some(ActualEntry::new(is_all_black, pos_before_area, self.z_index));
                } else {
                    actual_entry = Some(actual_entry.unwrap().advance(64));
                }
            };
            assembly_table.push(actual_entry.unwrap().to_assembly())
        }
        //insert empty entry
        assembly_table.push(ImageAssemblyEntry {
            pixel_src: 0,
            pixel_amount: 0,
            byte_amount: 0,
            _z_index: 0,
        });

        let assembly_table_offset = file.tell();

        //write assembly table
        let mut pointer = Vec::new();
        for entry in assembly_table {
            if entry.pixel_src != 0 {
                pointer.push(file.tell());
            };
            entry.write(&mut file).unwrap();
        };

        Ok((assembly_table_offset, pointer))
    }
}

pub struct ImageStore {
    pub images: Vec<Image>,
}

impl ImageStore {
    fn new_from_bytes(mut file: &mut Bytes, amount_images: u32, meta_frame_store: &MetaFrameStore, palette: &Palette) -> Result<ImageStore> {
        trace!("will read {} image", amount_images);
        let mut image_pointers: Vec<u64> = Vec::new(); //list of reference to image
        for _ in 0..amount_images {
            let current_pointer = file.read_u32_le()? as u64;
            if current_pointer == 0 {
                bail!("an image data pointer is null !!!");
            };
            image_pointers.push(current_pointer);
        };

        trace!("reading the image table");
        let mut images = Vec::new();

        for image_id in 0..image_pointers.len() {
            trace!("reading image n°{}", image_id);
            let resolution = match meta_frame_store.find_resolution_image(image_id as u32)? {
                Some(value) => value,
                None => bail!("the image does not have a resolution")
            };
            file.seek(image_pointers[image_id]);
            let img = Image::new_from_bytes(&mut file, resolution, &palette)?;
            images.push(img);
        }

        Ok(ImageStore{
            images: images,
        })
    }

    pub fn len(&self) -> usize {
        self.images.len()
    }

    fn write(mut file: &mut Bytes, wanimage: &WanImage) -> Result<(Vec<u64>, Vec<u64>)> {
        let mut image_offset = vec![];
        let mut sir0_pointer_images = vec![];

        for image in &wanimage.image_store.images {
            let (assembly_table_offset, sir0_img_pointer) = image.write(&mut file, &wanimage.palette).unwrap();
            for pointer in sir0_img_pointer {
                sir0_pointer_images.push(pointer)
            };
            image_offset.push(assembly_table_offset);
        }
        Ok((image_offset, sir0_pointer_images))
    }
}

pub struct Palette {
    pub palette: Vec<(u8,u8,u8,u8)>,
}

impl Palette {
    fn new_from_bytes(file: &mut Bytes) -> Result<Palette> {
        let mut palette = Vec::new();
        let pointer_palette_start = file.read_u32_le()? as u64;
        file.read(2)?;
        let nb_color = file.read_u16_le()?;
        file.read(4)?;
        trace!("palette_start: {}, nb_color: {}", pointer_palette_start, nb_color);
        if file.read_u32_le()? != 0 {
            bail!("the palette data doesn't end with 0s !!!");
        };
        file.seek(pointer_palette_start);
        for _ in 0..nb_color {
            let red = file.read_u8_le()?;
            let green = file.read_u8_le()?;
            let blue = file.read_u8_le()?;
            let alpha = file.read_u8_le()?;
            palette.push((red, green, blue, alpha));
        }
        Ok(Palette {
            palette: palette,
        })
    }

    fn get(&self, id: usize) -> Result<(u8,u8,u8,u8)> {
        if id >= self.palette.len() {
            bail!("impossible to get a color in the palette, as this will do an OOB.");
        };
        Ok(self.palette[id])
    }

    fn color_id(&self, target_color: (u8, u8, u8, u8)) -> Result<usize>{
        for color_id in 0..self.palette.len() {
            if self.palette[color_id] == target_color {
                return Ok(color_id)
            }
        }
        error!("impossible to find the palette {:?}", target_color);
        error!("tried :");
        for color in &self.palette {
            error!("{:?}", color);
        }
        bail!("impossible to find the specified color in the palette!");
    }

    fn write(&self, file: &mut Bytes) -> Result<u64> {
        let palette_start_offset = file.tell();
        for color in &self.palette {
            file.write_u8_le(color.0).unwrap();
            file.write_u8_le(color.1).unwrap();
            file.write_u8_le(color.2).unwrap();
            file.write_u8_le(color.3).unwrap();
        };

        let palette_header_offset = file.tell();
        file.write_u32_le(palette_start_offset as u32).unwrap();
        file.write_u16_le(0).unwrap(); //unk
        file.write_u16_le(16).unwrap(); //TODO: assume the picture is 4bit
        file.write_u32_le(0xFF << 16).unwrap(); //unk
        file.write_u32_le(0).unwrap(); //magic

        Ok(palette_header_offset)
    }
}

#[derive(Debug)]
struct AnimGroupEntry {
    pointer: u32,
    group_lenght: u16,
    _unk16: u16,
    id: u16,
}

#[derive(Debug, PartialEq)]
pub struct AnimationFrame {
    pub duration: u8,
    pub flag: u8,
    pub frame_id: u16,
    pub offset_x: i16,
    pub offset_y: i16,
    pub shadow_offset_x: i16,
    pub shadow_offset_y: i16,
}

impl AnimationFrame {
    fn new(file: &mut Bytes) -> Result<AnimationFrame> {
        let duration = file.read_u8_le()?;
        let flag = file.read_u8_le()?;
        let frame_id = file.read_u16_le()?;
        let offset_x = file.read_i16_le()?;
        let offset_y = file.read_i16_le()?;
        let shadow_offset_x = file.read_i16_le()?;
        let shadow_offset_y = file.read_i16_le()?;
        Ok(AnimationFrame {
            duration: duration,
            flag: flag,
            frame_id: frame_id,
            offset_x: offset_x,
            offset_y: offset_y,
            shadow_offset_x: shadow_offset_x,
            shadow_offset_y: shadow_offset_y,
        })
    }

    fn is_null(&self) -> bool {
        if self.duration == 0 && self.frame_id == 0 {
            true
        } else {
            false
        }
    }

    pub fn write(file: &mut Bytes, frame: &AnimationFrame) -> Result<()> {
        file.write_u8_le(frame.duration).unwrap();
        file.write_u8_le(frame.flag).unwrap();
        file.write_u16_le(frame.frame_id).unwrap();
        file.write_i16_le(frame.offset_x).unwrap();
        file.write_i16_le(frame.offset_y).unwrap();
        file.write_i16_le(frame.shadow_offset_x).unwrap();
        file.write_i16_le(frame.shadow_offset_y).unwrap();
        Ok(())
    }

    pub fn write_null(mut file: &mut Bytes) -> Result<()> {
        AnimationFrame::write(&mut file, &AnimationFrame{
            duration: 0,
            flag: 0,
            frame_id: 0,
            offset_x: 0,
            offset_y: 0,
            shadow_offset_x: 0,
            shadow_offset_y: 0,
        })
    }

}

#[derive(Debug, PartialEq)]
pub struct Animation {
    pub frames: Vec<AnimationFrame>,
}

impl Animation {
    fn new(mut file: &mut Bytes) -> Result<Animation> {
        let mut frames = Vec::new();
        loop {
            let current_frame = AnimationFrame::new(&mut file)?;
            if current_frame.is_null() {
                break;
            }
            frames.push(current_frame);
        }
        Ok(Animation{
            frames: frames
        })
    }

    pub fn len(&self) -> usize {
        return self.frames.len()
    }

    pub fn write(mut file: &mut Bytes, animation: &Animation) -> Result<()>{
        for frame in &animation.frames {
            AnimationFrame::write(&mut file, frame).unwrap();
        }
        AnimationFrame::write_null(&mut file).unwrap();
        Ok(())
    }
}

pub struct AnimStore {
    pub animations: Vec<Animation>,
    pub copied_on_previous: Option<Vec<bool>>, //indicate if a sprite can copy on the previous. Will always copy if possible if None
    pub anim_groups: Vec<Option<(usize, usize)>>, //usize1 = start, usize2 = lenght
}


impl AnimStore {
    fn new(mut file: &mut Bytes, pointer_animation_groups_table: u64, amount_animation_group: u16) -> Result<(AnimStore, u64)> {
        //TODO: rewrite this function, it seem to be too complicated to understand
        file.seek(pointer_animation_groups_table);
        let mut anim_group_entry: Vec<Option<AnimGroupEntry>> = Vec::new();
        for animation_group_id in 0..amount_animation_group+7 { //HACK: CRITICAL:
            let pointer = file.read_u32_le()?;
            if pointer == 0 {
                anim_group_entry.push(None);
                continue
            };
            let group_lenght = file.read_u16_le()?;
            let _unk16 = file.read_u16_le()?;
            anim_group_entry.push(Some(AnimGroupEntry{
                pointer: pointer,
                group_lenght: group_lenght,
                _unk16: _unk16,
                id: animation_group_id,
            }));
        }

        let mut anim_groups: Vec<Option<Vec<u64>>> = Vec::new();
        let mut particule_table_end = None;
        for anim_group_option in anim_group_entry {
            match anim_group_option {
                None => anim_groups.push(None),
                Some(anim_group) => {
                    file.seek(anim_group.pointer as u64);
                    match particule_table_end {
                        Some(value) => if file.tell() < value {
                            particule_table_end = Some(file.tell());
                        },
                        None => particule_table_end = Some(file.tell()),
                    };

                    let mut anim_ref = Vec::new();
                    for _ in 0..anim_group.group_lenght {
                        anim_ref.push(file.read_u32_le()? as u64);
                    }
                    trace!("reading an animation group entry, id is {}, the pointer is {:?}", anim_group.id, anim_ref);
                    anim_groups.push(Some(anim_ref));
                },
            };
        };
        if particule_table_end.is_none() {
            particule_table_end = Some(file.tell());
        };

        let mut animations: Vec<Animation> = Vec::new();
        let mut copied_on_previous = Vec::new();
        let mut anim_groups_result = Vec::new();
        let mut check_last_anim_pos = 0;
        for anim_group in anim_groups {
            match anim_group {
                None => anim_groups_result.push(None),
                Some(anim_group_table) => {
                    anim_groups_result.push(Some((animations.len(), anim_group_table.len())));
                    for animation in anim_group_table {
                        file.seek(animation);
                        if check_last_anim_pos > file.tell() {
                            bail!("The check for the order of animation haven't verified.")
                        };
                        copied_on_previous.push(file.tell() == check_last_anim_pos);
                        check_last_anim_pos = file.tell();
                        animations.push(Animation::new(&mut file)?);
                    }
                },
            };
        }


        Ok((AnimStore {
            animations,
            copied_on_previous: Some(copied_on_previous),
            anim_groups: anim_groups_result,
        }, particule_table_end.unwrap()))
    }

    pub fn len(&self) -> usize {
        return self.animations.len()
    }

    fn write(mut file: &mut Bytes, anim_store: &AnimStore) -> Result<Vec<u64>> {
        let mut animations_pointer = vec![];
        let mut previous_animation: Option<&Animation> = None;
        let mut previous_pointer = None;

        for loop_nb in 0..anim_store.animations.len() {
            let animation = &anim_store.animations[loop_nb];
            let can_copy_on_previous = match &anim_store.copied_on_previous {
                None => true,
                Some(value) => value[loop_nb],
            };
            let actual_pointer = file.tell();

            if can_copy_on_previous {
                match previous_animation  {
                    Some(p_anim) => if *p_anim == *animation {
                        animations_pointer.push(previous_pointer.unwrap());
                        continue
                    },
                    None => (),
                };
            };

            animations_pointer.push(actual_pointer);
            Animation::write(&mut file, animation).unwrap();
            previous_animation = Some(animation);
            previous_pointer = Some(actual_pointer);
        }
        Ok(animations_pointer)
    }

    fn write_animation_group(&self, file: &mut Bytes, animations_pointer: &Vec<u64>) -> Result<(u64, Vec<u64>)> {
        let mut sir0_animation = Vec::new();

        struct AnimGroupData {
            pointer: u32,
            lenght: u16,
        }

        let mut anim_group_data: Vec<Option<AnimGroupData>> = Vec::new();
        for anim_group in &self.anim_groups {
            match anim_group {
                None => {
                    anim_group_data.push(None);
                    file.write_u16_le(0).unwrap();
                },
                Some(value) => {
                    anim_group_data.push(Some(AnimGroupData {
                        pointer: file.tell() as u32,
                        lenght: value.1 as u16,
                    }));
                    for anim_pos in 0..value.1 {
                        sir0_animation.push(file.tell());
                        let value_to_write = animations_pointer[(value.0 as usize)+anim_pos] as u32;
                        file.write_u32_le(value_to_write).unwrap();
                    };

                }
            }
        };

        let animation_group_reference_offset = file.tell();

        for actual_data in anim_group_data {
            match actual_data {
                None => file.write_u32_le(0).unwrap(),
                Some(data) => {
                    sir0_animation.push(file.tell());
                    file.write_u32_le(data.pointer).unwrap();
                    file.write_u16_le(data.lenght).unwrap();
                    file.write_u16_le(0).unwrap();
                }
            };
        };


        Ok((animation_group_reference_offset, sir0_animation))
    }
}

pub struct WanImage {
    pub image_store: ImageStore,
    pub meta_frame_store: MetaFrameStore,
    pub anim_store: AnimStore,
    pub palette: Palette,
    pub raw_particule_table: Bytes,
    /// true if the picture have 256 color, false if it only have 16
    pub is_256_color: bool,
    pub sprite_type: SpriteType,
    pub unk_1: u32,
}

impl WanImage {
    pub fn new_from_bytes(b: Bytes) -> Result<WanImage> {
        WanImage::decode_wan(b).chain_err(|| "error creating a WanImage from bytes")
    }

    /// parse an image in the wan/wat format stored in the input file
    /// It assume that the file is decompressed
    pub fn decode_wan(mut file: Bytes) -> Result<WanImage> {
        debug!("start to decode a wan image");

        // first step: decode the sir0 header
        trace!("decoding the sir0 header");
        if file.read(4)? != vec!(0x53, 0x49, 0x52, 0x30) {
            bail!("the sir0 magic is not correct !!!");
        };
        let sir0_pointer_header = file.read_u32_le()? as u64;
        let _sir0_pointer_offset = file.read_u32_le()? as u64;
        if file.read(4)? != vec!(0,0,0,0) {
            bail!("what should four 0 isn't at the end of the sir0 header !!!");
        };

        // second step: decode the wan header
        trace!("reading the wan header");
        file.seek(sir0_pointer_header);
        let pointer_to_anim_info = file.read_u32_le()? as u64;
        let pointer_to_image_data_info = file.read_u32_le()? as u64;
        let sprite_type = match file.read_u16_le()? {
            0 => SpriteType::PropsUI,
            1 => SpriteType::Chara,
            3 => SpriteType::Unknown,
            _ => bail!("unknown type of sprite !!!"),
        };
        //unk #12
        if file.tell() != sir0_pointer_header+10 { // an assertion
            bail!("we are not at the good position after the wan header!!!");
        }


        // third step: decode animation info block
        trace!("reading the animation info block");
        file.seek(pointer_to_anim_info);
        let pointer_meta_frame_reference_table = file.read_u32_le()? as u64;
        let pointer_particule_offset_table = file.read_u32_le()? as u64;
        let pointer_animation_groups_table = file.read_u32_le()? as u64;
        let amount_animation_group = file.read_u16_le()?;

        if file.tell() != pointer_to_anim_info + 14 {
            bail!("we are not at the good position after the animation info block!!!");
        };
        let unk_1 = file.read_u32_le()?;

        // fourth: decode image data info
        trace!("reading the image data info");
        file.seek(pointer_to_image_data_info);
        let pointer_image_data_pointer_table = file.read_u32_le()? as u64;
        let pointer_palette = file.read_u32_le()? as u64;
        file.read(2)?; //unk
        let is_256_color = match file.read_u16_le()? {
            0 => false,
            1 => unimplemented!(),
            _ => bail!("the byte to indidicate the number of color of the sprite isn't 0 or 1!!!"),
        };
        file.read(2)?; //unk
        let amount_images = file.read_u16_le()?;


        trace!("parsing the palette");
        if pointer_palette == 0 {
            bail!("the palette pointer is equal to 0 !!!");
        };
        file.seek(pointer_palette);
        let palette = Palette::new_from_bytes(&mut file)?;

        // decode meta-frame
        trace!("decoding meta-frame");
        let meta_frame_reference_end_pointer: u64 = match pointer_particule_offset_table {
            0 => unimplemented!(),
            value => value,
        };
        let amount_meta_frame = (meta_frame_reference_end_pointer - pointer_meta_frame_reference_table)/4;
        file.seek(pointer_meta_frame_reference_table);
        let meta_frame_store = MetaFrameStore::new_from_bytes(&mut file, amount_meta_frame)?;

        // decode image
        trace!("reading the image data pointer table");
        file.seek(pointer_image_data_pointer_table);
        let image_store = ImageStore::new_from_bytes(&mut file, amount_images as u32, &meta_frame_store, &palette)?;

        // decode animation
        let (anim_store, particule_table_end) = AnimStore::new(&mut file, pointer_animation_groups_table, amount_animation_group)?;

        let raw_particule_table = file.create_sub_file(pointer_particule_offset_table, particule_table_end-pointer_particule_offset_table)?;
        Ok(WanImage{
            image_store: image_store,
            meta_frame_store: meta_frame_store,
            anim_store: anim_store,
            palette,
            raw_particule_table,
            is_256_color,
            sprite_type,
            unk_1,
        })
    }

    pub fn create_wan(wanimage: &WanImage) -> Result<Bytes> {
        //TODO: transform all unwrap to chain_error error
        let mut file = Bytes::default();
        debug!("start creating a wan image");

        let mut sir0_offsets = vec![];
        // create the sir0 header
        trace!("creating the sir0 header");
        file.write(vec!(0x53, 0x49, 0x52, 0x30)).unwrap(); // sir0 magic

        let sir0_pointer_header = file.tell();
        sir0_offsets.push(file.tell());
        file.write_u32_le(0).unwrap(); //sir0_pointer_header

        let sir0_pointer_offset = file.tell();
        sir0_offsets.push(file.tell());
        file.write_u32_le(0).unwrap();

        file.write_u32_le(0).unwrap(); // magic

        // write meta-frame
        trace!("start of meta frame reference: {}", file.tell());
        let meta_frame_references = MetaFrameStore::write(&mut file, &wanimage.meta_frame_store).unwrap();

        trace!("start of the animation offset: {}", file.tell());
        let animations_pointer = AnimStore::write(&mut file, &wanimage.anim_store).unwrap();

        file.write_padding(0xAA, 4).unwrap();

        trace!("start of the image offset: {}", file.tell());
        let (image_offset, sir0_pointer_images) = ImageStore::write(&mut file, &wanimage).unwrap();

        for pointer in sir0_pointer_images {
            sir0_offsets.push(pointer);
        };


        trace!("start of the palette: {}", file.tell());
        let pointer_palette = wanimage.palette.write(&mut file).unwrap();
        //sir0_offsets.push(pointer_palette);

        sir0_offsets.push(pointer_palette);

        trace!("start of the meta_frame reference offset: {}", file.tell());
        let meta_frame_reference_offset = file.tell();
        for reference in meta_frame_references {
            sir0_offsets.push(file.tell());
            file.write_u32_le(reference).unwrap();
        };

        let particule_offset = file.tell();
        trace!("start of the particule offset: {}", file.tell());
        //HACK: particule offset table parsing is not implement (see the psycommand code of spriteditor)
        file.write_bytes(&wanimage.raw_particule_table).unwrap();
        sir0_offsets.push(file.tell());

        trace!("start of the animation group reference: {}", file.tell());
        let (animation_group_reference_offset, sir0_animation_pointer) = wanimage.anim_store.write_animation_group(&mut file, &animations_pointer).unwrap();
        for pointer in sir0_animation_pointer {
            sir0_offsets.push(pointer);
        };

        //image offset
        let pointer_image_data_pointer_table = file.tell();
        trace!("start of the image offset: {}", file.tell());
        for offset in image_offset {
            sir0_offsets.push(file.tell());
            file.write_u32_le(offset as u32).unwrap();
        };

        // animation header
        let animation_info_offset = file.tell();
        trace!("start of the animation header: {}", file.tell());
        sir0_offsets.push(file.tell());
        file.write_u32_le(meta_frame_reference_offset as u32).unwrap();
        sir0_offsets.push(file.tell());
        file.write_u32_le(particule_offset as u32).unwrap();
        sir0_offsets.push(file.tell());
        file.write_u32_le(animation_group_reference_offset as u32).unwrap();
        file.write_u16_le((wanimage.anim_store.anim_groups.len()-7) as u16).unwrap(); //HACK:

        // HACK: check what does this mean
        file.write_u32_le(wanimage.unk_1).unwrap();
        file.write_u32_le(0).unwrap();
        file.write_u16_le(0).unwrap();

        // images header
        trace!("start of the images header: {}", file.tell());
        let image_info_offset = file.tell();
        sir0_offsets.push(file.tell());
        file.write_u32_le(pointer_image_data_pointer_table as u32).unwrap();
        sir0_offsets.push(file.tell());
        file.write_u32_le(pointer_palette as u32).unwrap();
        file.write_u16_le(0).unwrap(); //HACK: unknow
        file.write_u16_le(if wanimage.is_256_color {
            1
        } else {
            0
        }).unwrap();

        file.write_u16_le(1).unwrap(); //HACK: unknow
        file.write_u16_le(wanimage.image_store.len() as u16).unwrap();

        // wan header
        let wan_header_pos = file.tell();
        sir0_offsets.push(file.tell());
        file.write_u32_le(animation_info_offset as u32).unwrap();
        sir0_offsets.push(file.tell());
        file.write_u32_le(image_info_offset as u32).unwrap();
        file.write_u16_le(wanimage.sprite_type.get_id() as u16).unwrap();


        file.write_u16_le(0).unwrap();

        file.write_padding(0xAA, 32).unwrap();

        let sir0_offset_pos = file.tell();
        // write the sir0 ending

        trace!("start of the sir0 list: {}", file.tell());
        Sir0::write_offset_list(&mut file, &sir0_offsets).unwrap();

        file.write_padding(0xAA, 32);

        // write the sir0 header
        file.seek(sir0_pointer_header);
        file.write_u32_le(wan_header_pos as u32).unwrap();

        file.seek(sir0_pointer_offset);
        file.write_u32_le(sir0_offset_pos as u32).unwrap();

        file.seek(0);
        Ok(file)
    }
}
