# wan
The wan file format is the one who is used to store sprite and evironnemnent. They have either the .wan or the .wat format, but others can be found in CPack's file.

They are always in a SIR0 file.

## the format
### parts of the file
from psycommando (name may differ)

|offset|lenght|name|description|
|:--------:|:--------:|:------:|:-------------:|
|0x00|16|[SIR0 header](#SIR0-header)|The SIR0 container's header. The first pointer points to the [WAN Header](#WAN-header). The second to the [SIR0 pointer list](#SIR0-pointer-list).|
|0x10|varies|[meta-frames](#meta-frames)|Contain the meta-frames data.|
|just next|varies|[animation](#animation)|Contain the animation data.|
|just next|varies|padding (if needed)| Padding of 0xAA to align to 4 bytes.|
|just next|varies|[images](#images)|Contain the images.|
|just next|varies|[palette](#palette)|Contain the palette of the file.|
|just next|varies|[meta-frames references](#meta-frames-references)|A list of pointer to [meta-frames](#meta-frames)|
|just next|varies|[particules offset](#particules-offset)|Indicate the offset to the screen of particules effects|
|just next|varies|[animation sequences references](#animation-sequences-references)|Put multiple animation into one animated animation sequence|
|just next|varies|[animation sequences references references](#animation-sequences-references-references)|Reference to entrys of the [animation sequences references](#animation-sequences-references)|
|just next|varies|[animation references](#animation-references)|references to animation|
|just next|varies|[images references](#images-references)|references to [images](#images)|
|just next|varies|[animations header](#animation-header)|contain information about animations|
|just next|varies|[images header](#image-header)|contain information about images|
|just next|varies|[WAN header](#WAN-header)|contain pointer to other header of the file, plus some data|
|just next|varies|padding|0xAA padding to align to 16 Bytes|
|just next|varies|SIR0 encoded pointer list|low level data for the nds|
|just next|varies|padding|0xAA to align to 16 Bytes|


### images
An image (which is not always a full image, as they need to be assembled with [meta-frames](#meta-frames)) have a limited resolution size.

The image id is the number of the image, starting at zero, and are incremented by one for each image (in the order of the image reference table).

We need to know the resolution of the image first. To do that we need to search in meta-frames to see which one have a resolution for this image id.

We then need to know how images are encoded: There are two step for this.

At first, there are a compression algoritm which delete data about transparents parts of the image. That will give us an 1d table of u4 or u8 (depending on the palette lenght).

After this, we need to place the pixel on the image, who is divided by tile of 8x8 pixel.

#### decompression of the image
The compression data is where the pointer in the [images references](#images references) direct to.

First we need to parse the image assembly entry. An image assembly entry have a lenght of 12 Bytes:

__image assembly entry__

|offset (relative)|type(lenght)|name        |description                                                      |
|:---------------:|:----------:|:----------:|:---------------------------------------------------------------:|
|0x00             |u32_le(4B)  |pixel source|the pointer to the beggining of this pixel source (or 0 if transparent)|
|0x04             |u16_le(2B)  |byte amount |the lenght of pixel, in __Bytes__ (not pixel)                    |
|0x06             |unk(2B)     |unknown     |unknown                                                          |
|0x08             |u32_le(4B)  |z index     |unknwon                                                          |

Once the image assembly entry are parsed, we need to use them. Here is a small algo to decompress them (in rust):

```rust,noplaypen
struct AssemblyEntry {
	pixel_source: u32,
	byte_amount: u16,
}

# fn get_file() -> Vec<u8> {
#   return vec![10;200];
# }

pub fn main (){
	let file: Vec<u8> = get_file();
	let mut assembly_list: Vec<AssemblyEntry> = Vec::new();
	// parse the assembly entry ...
#     assembly_list.push(AssemblyEntry{pixel_source: 100, byte_amount: 32});
#     assembly_list.push(AssemblyEntry{pixel_source: 0, byte_amount: 64});
#     assembly_list.push(AssemblyEntry{pixel_source: 0, byte_amount: 0});

	let mut decompressed: Vec<u8> = Vec::new();
	for assembly in assembly_list {
		if assembly.byte_amount == 0 {break;}; // detect the end of the assembly entry
		if assembly.pixel_source == 0 {
			// fill with black
			for _ in 0..assembly.byte_amount {
				decompressed.push(0);
			}
		} else {
			// copy
			for byte in assembly.pixel_source..assembly.pixel_source + (assembly.byte_amount as u32) {
				decompressed.push(file[byte as usize]);
			}
		}
	}
# let mut should_be = Vec::new();
# for _ in 0..32 {
#   should_be.push(10);
# }
# for _ in 0..64 {
#   should_be.push(0);
# }
# assert_eq!(should_be, decompressed);
	// the decompressed file is now in "decompressed"
}
```

#### placing the pixel (THIS PART IS NOT CORRECT !!!)

The algoritm to place pixel on the good coordinate seem a bit strange (and I'm curious to know why).

I will explain here how to do this based on input (first place the first decompressed pixel, then the second...) rather than the other order (place the pixel at 0,0, then 0,1, ... 1,0, then 1,1 ... ).

Also, remember to take care we speak here in pixel and not byte, and pixel can be eiter 4 or 8 bit depending on the number of avalaible color.

The image is divided in tile of 8x8 pixel, so the first 64 (8x8) pixel will be at the upper right, the next 64 will be a tile downer. Once the end is reached, we return to the upper one to the right.

![tile placement](./wan-image/tile-order.svg)

To parse a tile, there is a quite strange order: We parse the file by column from up to down (by 8 pixel), then, we go up again, until we parsed the 8 column. However, we first need to parse the second column, then the first, then the fourth, then the third... etc.

|column in the wan|column in the final picture|
|:--:|:--:|
|0   |1   |
|1   |0   |
|2   |3   |
|3   |2   |
|4   |5   |
|5   |4   |
|6   |7   |
|7   |6   |

![pixel placement in a tile](./wan-image/pixel-order.svg)

//TODO: example
#### number of images
TODO
