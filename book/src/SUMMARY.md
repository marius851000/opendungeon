# Summary

* [information](./information.md)
* [game-modding](./game-modding/index.md)
    * [3ds](./game-modding/3ds/index.md)
        * [lua script](./game-modding/3ds/script/lua.md)
          * [lua useful function](./game-modding/3ds/script/lua-function.md)
            * [lua talk function](./game-modding/3ds/script/lua-function/talk.md)
        * [script list](./game-modding/3ds/script/script_entry_list.md)
    * [terminal quick guide](./game-modding/terminal.md)
* [file format](./file-format/index.md)
    * [wan](./file-format/wan.md)
