# 3ds modding general consideration
In this guide, I'll assume you already have a tool to patch the game.
3ds games use a filesystem, that contain all of the game data. It is named romfs, as opposed to exefs, that contain the code (there are also other things to do a full 3ds game, but that's all you'll need to create a good mod).

## getting the romfs
To extract this game, you'll first need a decrypted rom (the 3ds use DRM to protect against piracy). There are multiple way to find one. First, you can go on a piracy site and download a decrypted .3ds file (search on the-eye.eu for this). You can also extract the game if you own the cartbridge or have the eshop version. You'll need a hacked 3ds with a CFW.

For more information, search on internet.

## moddifying the romfs
There are also two main way to modify the romfs of a game. The recommand one is with the luma 3ds virtual file system (again, search on internet). I use a ftp program to transfer mod from my computer to the 3ds (there are homebrew program that allow this). You can also repack to a .3ds file or a .cia file, and use an emulator, a 3ds linker, or even install it on your 3ds. Again, search on internet.
