# lua scripting
PSMD and GTI games use the lua programming language for it's ingame cinematic. It's a widely used language for this kind of stuff. You'll first need to learn lua to understand what follow here.

The lua scripts are stored at the various subfolder of romfs/script. They all have the .lua extension, but a compressed with ``luac``. You may need to unpack those with the unluac java program (or anything similar) to obtain a normal lua file, althrought they doesn't keep there original syntax, includings commentary. However, the 3ds game engine is still able to run uncompressed lua file, so you don't need to recompile them.

I offer a repository with all the script from the EU version of PSMD on github (TODO: put the link).

A good tool to check if the syntax is correct is ``luac``. You just need to run (in a [terminal prompt](../../terminal.md)) ``luac -p filename``. It will succed if your file is well formatted, but will fail otherwise (and will inform you why). Please it only check the syntax, not that command are valids.

## useful function
see this [list of lua useful lua function](./lua-function.md)

## next cinematic
I am not actually able to change the cinematic that come after another one. I have some hope that this is stored in the ``script_flow.bin`` and related file.
