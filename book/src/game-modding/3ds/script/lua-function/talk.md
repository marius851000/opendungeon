# lua talk function

## WINDOW:Talk
This code open a talk dialog, as used all the way throught the story. It will draw the face of the talking personage if it has been previously specified.

The syntax to call it for custum lua script is : ``WINDOW:Talk(SymAct("character"), "text")``

character should be replaced by the id of the character who speak. Usually, ``HERO`` and ``PARTER`` are good choice.
text should be text he say. Please note that space should be replaced by this (japanese ?) character: ``쐃`` (you can just copy-paste it).

In the game script, you'll also see things like ``WINDOW:Talk(SymAct("character"), number)``, where number is a positive or negative one.

That is because the number reference here to the id of the string, who is stored in another file. This is useful for translation, as there is one string file per language.
