# script list

WARNING: this is not yet tested, and information here can be false

The script list of the game is stored in the script_entry_list.bin file, who is in the romfs/script folder.

This is a [command line](../terminal.md) tool.

the tool is named ``bin_script_entry_list``. It is written by marius851000 in rust.

to transform a ``script_entry_list.bin`` to an easy to modify ``.json`` file, you need to run ``bin_script_entry_list -i inputfile -o outputfile tojson`` where ``inputfile`` is the bin file and ``outputfile`` is the path to the json file who will be created (with it's extension).

to transform a ``.json`` file to ``script_entry_list.bin`` file, run ``bin_script_entry_list -i inputfile -o outputfile tobin``, where ``inputfile`` is the ``.json`` file, and ``outputfile`` is the target ``script_entry_list.bin`` file. Remember to not override the original file, and rather create a new one you will add to your patch.

The json is organised like this (you may want to use ``cat jsonfile | jq > otherjsonfile`` to pretty print it):
```json
{
	"scripts": [
		{
			"id1": "hanyou_tsuginoasa01",
			"id2": "hanyou_tsuginoasa01",
			"lua_file": "event/hanyou/tsuginoasa01/tsuginoasa.lua",
			"plb_name": "hanyou_tsuginoasa01.plb",
			"flag_str": "0000100010000000"
		},
		...
	],
	"order": [
		"hanyou_chourei01-hanyou_chourei01",
		"hanyou_yuuhan01-hanyou_yuuhan01",
		...
	]
}
```
