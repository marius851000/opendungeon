extern crate common;
extern crate serde_json;
extern crate clap;
use common::{Bytes, RABytes};
use std::path::PathBuf;
use std::path::Path;
use std::{fs::File, io::Write};
use bin_script_entry_list::{parse_entry_list,generate_entry_list,ScriptList};
use clap::{App, SubCommand, Arg};


fn main() {
    let matches = App::new("script_entry_list.bin read/write")
        .arg(Arg::with_name("input")
            .short("i")
            .value_name("input")
            .required(true))
        .arg(Arg::with_name("output")
            .short("o")
            .value_name("output")
            .required(true))
        .subcommand(SubCommand::with_name("tojson")
            .about("transform a .bin to a .json file"))
        .subcommand(SubCommand::with_name("tobin")
            .about("transform a .json to a .bin file"))
        .get_matches();

    let input_file = Bytes::new_from_path(
        &PathBuf::from(matches.value_of("input").unwrap())
    ).unwrap();

    let out_path = Path::new(matches.value_of("output").unwrap());

    if matches.subcommand_matches("tojson").is_some() {
        let result = parse_entry_list(input_file).unwrap();
        let serialized = serde_json::to_string(&result).unwrap();

        let mut write_file = File::create(&out_path).unwrap();
        write_file.write_all(&Vec::from(serialized)).unwrap();
    } else if matches.subcommand_matches("tobin").is_some() {
        let deserialized: ScriptList = serde_json::from_slice(input_file.read_all_vec().as_slice()).unwrap();
        let file = generate_entry_list(deserialized);

        let mut write_file = File::create(&out_path).unwrap();
        write_file.write_all(&file.read_all_vec()).unwrap();
    } else {
        panic!("a subcommand was awaited!")
    }
}
