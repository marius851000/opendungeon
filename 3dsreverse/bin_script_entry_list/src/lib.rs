extern crate common;
extern crate serde;
extern crate sir0;
use common::Bytes;
use common::RABytes;
use sir0::Sir0;
use serde::{Serialize,Deserialize};
use std::collections::HashMap;

//TODO: chain_err

#[derive(Debug, Serialize, Deserialize)]
pub struct ScriptEntry {
    pub id1: String,
    pub id2: String,
    pub lua_file: String,
    pub plb_name: String,
    pub flag_str: String,
}

#[derive(Debug, Serialize, Deserialize)]
pub struct ScriptList {
    pub scripts: Vec<ScriptEntry>,
    pub order: Vec<String>,

}

#[allow(clippy::cast_lossless)]
pub fn parse_entry_list(file: Bytes) -> Result<ScriptList, &'static str> {
    fn decode_u16_zero_terminated_string(file: &mut Bytes) -> Result<String, &'static str> {
        let mut str = String::new();
        loop {
            let chara = file.read_u16_le()?;
            if chara == 0 {break;};
            str.push((chara as u8) as char); //TODO: not as
        };
        Ok(str)
    }

    let s = Sir0::new_from_bytes(file)?;
    let mut file = s.get_file();


    let number_of_script = file.read_u32_le()?;
    if s.len() <= 3 {
        return Err("no enought pointer");
    };

    file.seek(s.get_pointer(1) as u64); //TODO:
    let begining_map_content_pointer = file.read_u32_le()? as u64 - 16;
    let mut unk_map = HashMap::new();
    let mut loop_nb = 0;
    while file.tell() < begining_map_content_pointer {
        let key = file.read_u32_le()?;
        unk_map.insert(key, loop_nb);
        loop_nb += 1;
    }

    let mut content = Vec::new();
    let mut order: Vec<String> = vec![String::from(""); unk_map.len()];

    for _ in 0..number_of_script {
        let section_beggining = file.tell();

        let id_1_pointer = file.read_u32_le()? as u64;
        let id_2_pointer = file.read_u32_le()? as u64;
        let lua_file_pointer = file.read_u32_le()? as u64;
        let plb_name_pointer = file.read_u32_le()? as u64;
        let flag_pointer = file.read_u32_le()? as u64;
        let saved_position = file.tell();

        file.seek(id_1_pointer-16);
        let id1 = file.read_string_ascii_null_terminated()?;

        file.seek(id_2_pointer-16);
        let id2 = file.read_string_ascii_null_terminated()?;

        file.seek(lua_file_pointer-16);
        let lua_file = decode_u16_zero_terminated_string(&mut file)?;

        file.seek(plb_name_pointer-16);
        let plb_name = decode_u16_zero_terminated_string(&mut file)?;

        file.seek(flag_pointer-16);
        //TODO: do not use str
        let temp = file.read(16)?;
        let mut flag_str = String::new();
        for l in temp {
            flag_str.push(match l {
                0=>'0',
                1=>'1',
                2=>'2',
                _=>'_',
            });
        };

        if unk_map.contains_key(&((section_beggining+16) as u32)) {
            let position = unk_map[&((section_beggining+16) as u32)];
            if !position >= order.len() {
                let mut identifier = id1.clone();
                identifier.push('-');
                identifier.push_str(&id2.clone());
                order[position] = identifier;
            } else {
                return Err("do not have enought place in order to store a script");
            };
        }

        file.seek(saved_position);
        content.push(
            ScriptEntry {
                id1,
                id2,
                lua_file,
                plb_name,
                flag_str,
            }
        );
    }

    Ok(ScriptList {
        scripts: content,
        order,
    })
}

pub fn generate_entry_list(content: ScriptList) -> Bytes {
    fn write_ascii_string_16(file: &mut Bytes, str: &str) -> Result<(), &'static str>{
        for char in str.chars() {
            file.write_u16_le(char as u16)?;
        }
        file.write_u16_le(0)?;
        Ok(())
    }
    let mut file = Bytes::new();
    //sir0 header
    file.write_vec(b"SIR0".to_vec()).unwrap();
    file.write_u32_le(16).unwrap();
    file.write_u32_le(0).unwrap(); //pointer to offset list. should be rewritten
    file.write_u32_le(0).unwrap();

    //another header
    file.write_u32_le(content.scripts.len() as u32).unwrap();
    file.write_u32_le(24).unwrap(); //TODO: understand why
    let pointer_map_content_pointer = file.tell();
    file.write_u32_le(0).unwrap(); //pointer to the first script data. should be rewrittent

    // unknwown data
    let placement_data_pointer = file.tell();
    let mut placement_data = HashMap::new();
    for l in 0..content.order.len() {
        placement_data.insert(&content.order[l], l);
    }

    for _ in 0..content.order.len() {
        file.write_u32_le(0).unwrap();
    }

    // rewrite the pointer to here
    let pointer_start_map_data = file.tell();
    file.seek(pointer_map_content_pointer);
    file.write_u32_le(pointer_start_map_data as u32).unwrap();
    file.seek(pointer_start_map_data);

    // write map content
    for _ in 0..content.scripts.len() {
        for _ in 0..5 {
            file.write_u32_le(0).unwrap();
        }
    }

    // write map data
    let mut content_pointer: Vec<u32> = Vec::new();
    for map in &content.scripts {
        assert_eq!(map.flag_str.len(), 16);
        content_pointer.push(file.tell() as u32);
        for chara in map.flag_str.chars() {
            file.write_u8_le(match chara {
                '0' => 0,
                '1' => 1,
                '2' => 2,
                _ => panic!(),
            }).unwrap();
        }
    }

    // write lua and plb
    let mut already_included_strings = HashMap::new();
    let mut lua_pointer: Vec<u32> = Vec::new();
    let mut plb_pointer: Vec<u32> = Vec::new();
    for map in &content.scripts {
        if !already_included_strings.contains_key(&map.lua_file) {
            already_included_strings.insert(&map.lua_file, file.tell() as u32);
            write_ascii_string_16(&mut file, &map.lua_file).unwrap();
        };
        lua_pointer.push(already_included_strings[&map.lua_file]);

        if !already_included_strings.contains_key(&map.plb_name) {
            already_included_strings.insert(&map.plb_name, file.tell() as u32);
            write_ascii_string_16(&mut file, &map.plb_name).unwrap();
        };
        plb_pointer.push(already_included_strings[&map.plb_name]);
    }

    // write id1 and id2
    let mut already_included_strings = HashMap::new();
    let mut id_1_pointer = Vec::new();
    let mut id_2_pointer = Vec::new();
    for map in &content.scripts {
        //TODO: generalize the 5 next line
        if !already_included_strings.contains_key(&map.id1) {
            already_included_strings.insert(&map.id1, file.tell() as u32);
            file.write_ascii_string_null_terminated(&map.id1).unwrap();
        }
        id_1_pointer.push(already_included_strings[&map.id1]);

        if !already_included_strings.contains_key(&map.id2) {
            already_included_strings.insert(&map.id2, file.tell() as u32);
            file.write_ascii_string_null_terminated(&map.id2).unwrap();
        }
        id_2_pointer.push(already_included_strings[&map.id2]);
    }
    let end_of_actual_file = file.tell();

    // rewrite the old value
    let mut read_placement_data = HashMap::new();
    file.seek(pointer_start_map_data);

    for l in 0..content.scripts.len() {
        let mut id = content.scripts[l].id1.clone();
        id.push('-');
        id.push_str(&content.scripts[l].id2);
        if placement_data.contains_key(&id) {
            read_placement_data.insert(placement_data[&id], file.tell());
        };
        file.write_u32_le(id_1_pointer[l]).unwrap();
        file.write_u32_le(id_2_pointer[l]).unwrap();
        file.write_u32_le(lua_pointer[l]).unwrap();
        file.write_u32_le(plb_pointer[l]).unwrap();
        file.write_u32_le(content_pointer[l]).unwrap();
    }

    file.seek(placement_data_pointer);
    for l in 0..content.order.len() {
        if read_placement_data.contains_key(&l) {
            file.write_u32_le(read_placement_data[&l] as u32).unwrap();
        } else {
            panic!();
        };
    }

    file.seek(8);
    file.write_u32_le(end_of_actual_file as u32 + 2).unwrap();

    // extra SIR0 data
    file.seek(end_of_actual_file);
    file.write_u8_le(0).unwrap();
    file.write_u8_le(0).unwrap(); // I think this is padding
    file.write_u8_le(4).unwrap();
    file.write_u8_le(4).unwrap();
    file.write_u8_le(12).unwrap(); //end of header + 4

    #[allow(clippy::range_plus_one)]
    for _ in 0..(content.scripts.len()*5 +content.order.len()+1) {
        file.write_u8_le(4).unwrap();
    }
    file.write_u8_le(0).unwrap();

    //TODO: write the pointer to the offset list
    file
}
