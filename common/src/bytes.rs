use crate::rabytes::RABytes;
use crate::get_bit;
use std::path::PathBuf;
use std::fs::File;
use std::io::prelude::*;

#[derive(Debug, Clone, Default)]
/// Allow to access a vector of byte (u8), with a pointer to the actual data.
pub struct Bytes {
    /// The bytes of the file
    bytes: Vec<u8>,
    /// number of bit since the start (not bytes)
    pointer: u128,
}

impl Bytes {
    /// create a new empty [`Bytes`] object
    ///
    /// # Examples
    ///
    /// ```
    /// use opendungeon_common::Bytes;
    /// let bytes = Bytes::new();
    /// ```
    pub fn new() -> Bytes {
        Bytes::default()
    }

    /// create a new [`Bytes`] object with the content of a Vec<u8>
    ///
    /// # Examples
    ///
    /// ```
    /// use opendungeon_common::{Bytes, RABytes};
    /// let mut bytes = Bytes::new_from_vec(vec![10,10,10,10]);
    /// assert_eq!(bytes.read(2).unwrap(), vec![10,10]);
    /// ```
    pub fn new_from_vec(vec: Vec<u8>) -> Bytes {
        Bytes{bytes:vec, pointer:0}
    }

    /// create a new ['Bytes'] object containing the content of the file stored at the path file_path
    pub fn new_from_path(file_path: &PathBuf) -> std::io::Result<Bytes> {
        let mut file = File::open(file_path)?;
        let mut buf: Vec<u8> = Vec::new();
        file.read_to_end(&mut buf)?;
        Ok(Bytes::new_from_vec(buf))
    }

    /// transform into a string, using the ascii code. Mainly used for debug display
    ///
    /// # Examples
    ///
    /// ```
    /// use opendungeon_common::{Bytes, RABytes};
    /// let mut bytes = Bytes::new_from_vec(b"hello".to_vec());
    /// assert_eq!(bytes.as_string(), String::from("hello")); // String::from("hello") is a String
    /// ```
    pub fn as_string(&self) -> String {
        let mut str = String::new();
        for c in &self.bytes {
            str.push(char::from(*c));
        }
        str
    }
}

impl RABytes for Bytes {
    /// return the lenght of data stored in this [`Bytes`], in number of bytes
    ///
    /// # Examples
    ///
    /// ```
    /// use opendungeon_common::{Bytes, RABytes};
    /// let bytes = Bytes::new_from_vec(vec![10,10,10]);
    /// assert_eq!(bytes.len(), 3);
    /// ```
    #[allow(clippy::len_without_is_empty)]
    fn len(&self) -> u64 {
        self.bytes.len() as u64 // not computer with more than 2^64 B of ram should exist in the foreseable future.
    }

    /// return the current position of the pointer
    ///
    /// # Examples
    ///
    /// ```
    /// use opendungeon_common::{Bytes, RABytes};
    /// let mut bytes = Bytes::new_from_vec(b"Hello, World!".to_vec());
    /// bytes.seek(5);
    /// assert_eq!(bytes.tell(), 5);
    /// bytes.read(3).unwrap();
    /// assert_eq!(bytes.tell(), 8);
    /// ```
    fn tell_bit(&self) -> u128 {
        self.pointer
    }

    /// read a number of bytes from the pointer and return an Result<Vec<u8>, &'static str>. Increase the pointer.
    ///
    /// if it read (even for one bytes) out of the bound, it return None.
    ///
    /// # Examples
    ///
    /// ```
    /// use opendungeon_common::{Bytes, RABytes};
    /// let mut bytes = Bytes::new_from_vec(b"hello".to_vec());
    /// assert_eq!(bytes.read(5).unwrap(), b"hello");
    /// ```
    fn read(&mut self, lenght: u64) -> Result<Vec<u8>, &'static str> {
        if self.tell_bit() + u128::from(lenght*8) > self.len_bit() {
            return Err("impossible to read enought byte in Bytes")
        };
        if self.is_aligned() {
            let mut result = Vec::new();
            for position in self.tell()..self.tell()+lenght {
                result.push(self.bytes[position as usize]);
            }
            self.pointer += u128::from(lenght*8);
            Ok(result)
        } else {
            let mut result = Vec::new();
            for _ in 0..lenght {
                let next_8_bit = self.read_bit(8)?;
                let mut actual_byte = 0;
                for l in 0..8 {
                    actual_byte <<= 1;
                    actual_byte += if next_8_bit[l] { 1 } else { 0 };
                }
                result.push(actual_byte);
            }
            Ok(result)
        }
    }

    /// read a number of bit...
    ///
    /// # Examples
    ///
    /// ```
    /// use opendungeon_common::{Bytes, RABytes};
    ///
    /// let mut bytes = Bytes::new_from_vec(vec![0x0F, 0xF0]); //0000 1111 1111 0000
    /// bytes.seek_bit(7);
    /// assert_eq!(bytes.read_bit(8).unwrap(), vec![true, true, true, true, true, false, false, false]);
    /// assert_eq!(bytes.tell_bit(), 15);
    /// ```
    fn read_bit(&mut self, lenght: u128) -> Result<Vec<bool>, &'static str> {
        if self.tell_bit() + lenght > self.len_bit() {
            return Err("impossible to read renough bit without going out of bound.");
        };
        let mut result = Vec::new();
        for x in self.tell_bit()..self.tell_bit() + lenght {
            let actual_byte = self.get_byte((x / 8) as u64)?;
            let bit_position = x%8;
            result.push(get_bit(actual_byte, bit_position as usize).unwrap()); // should be safe
        }
        self.pointer += lenght;
        Ok(result)
    }

    /// get the byte at the given position. Return an error if outside of the file.
    ///
    /// # Examples
    ///
    /// ```
    /// use opendungeon_common::{Bytes, RABytes};
    /// let bytes = Bytes::new_from_vec(vec![10,5,4,6]);
    /// assert_eq!(bytes.get_byte(2).unwrap(), 4)
    /// ```
    fn get_byte(&self, pos: u64) -> Result<u8, &'static str> {
        if pos >= self.len() {
            return Err("impossible to read a byte without overflow !!!");
        };
        Ok(self.bytes[pos as usize])
    }

    /// set the position of the pointer. Succeed even if the cursor is after the end of the file (read will fail)
    ///
    /// # Examples
    ///
    /// ```
    /// use opendungeon_common::{Bytes, RABytes};
    /// let mut bytes = Bytes::new_from_vec(b"hello".to_vec());
    /// bytes.seek(0);
    /// assert_eq!(bytes.read(5).unwrap(), b"hello");
    /// ```
    ///
    /// ```
    /// use opendungeon_common::{Bytes, RABytes};
    /// let mut bytes = Bytes::new();
    /// bytes.seek(10); //doesn't panic
    /// ```
    fn seek_bit(&mut self, position: u128) {
        self.pointer = position;
    }

    /// set a byte at a given position to a given value
    ///
    /// # Examples
    ///
    /// ```
    /// use opendungeon_common::{Bytes, RABytes};
    /// let mut bytes = Bytes::new_from_vec(vec![0,1,2]);
    /// bytes.set_byte(1, 5).unwrap();
    /// assert_eq!(bytes.read_all_vec(), vec![0,5,2]);
    /// ```
    fn set_byte(&mut self, pos: u64, value: u8) -> Result<(), &'static str> {
        if pos >= self.len() {
            return Err("impossible to set this byte that is outside of the file.")
        } else {
            self.bytes[pos as usize] = value;
            Ok(())
        }
    }

    /// append a byte with the given value at the end of the file. Always suceed
    ///
    /// # Examples
    ///
    /// ```
    /// use opendungeon_common::{Bytes, RABytes};
    /// let mut bytes = Bytes::new_from_vec(vec![0,1]);
    /// bytes.add_a_byte(2).unwrap(); // always suceed
    /// assert_eq!(bytes.read_all_vec(), vec![0,1,2]);
    /// ```
    fn add_a_byte(&mut self, value: u8) -> Result<(), &'static str> {
        self.bytes.push(value);
        Ok(())
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_bytes_new_from_vec() {
        let bytes = Bytes::new_from_vec(vec![]);
        assert_eq!(bytes.len(),0);
    }
    #[test]
    fn test_bytes_as_string() {
        let bytes = Bytes::new_from_vec(b"".to_vec());
        assert_eq!(bytes.as_string(), String::from(""));
    }
    #[test]
    fn test_bytes_len() {
        let bytes = Bytes::new_from_vec(vec![]);
        assert_eq!(bytes.len(),0);
        let bytes = Bytes::new_from_vec(vec![10;10]);
        assert_eq!(bytes.len(),10)
    }
    #[test]
    fn test_bytes_get_byte() {
        let bytes = Bytes::new_from_vec(vec![1,2,3]);
        assert_eq!(bytes.get_byte(0),Ok(1));
        assert_eq!(bytes.get_byte(2),Ok(3));
        assert!(bytes.get_byte(3).is_err());
        assert!(bytes.get_byte(10).is_err());
    }
    #[test]
    fn test_bytes_tell_bit() {
        let mut bytes = Bytes::new_from_vec(vec![1,0]);
        assert_eq!(bytes.tell_bit(),0);
        bytes.seek_bit(10);
        assert_eq!(bytes.tell_bit(),10);
        bytes.seek(1);
        assert_eq!(bytes.tell_bit(),8);
        bytes.seek_bit(100);
        assert!(bytes.read(1).is_err());

    }
    #[test]
    fn test_bytes_read() {
        let mut bytes = Bytes::new_from_vec(vec![0b01100011,0b11110000,0xFF]);
        assert_eq!(bytes.read(1).unwrap(),vec![0b01100011]);
        assert_eq!(bytes.read(2).unwrap(),vec![0b11110000,0xFF]);
        assert!(bytes.read(1).is_err());
        bytes.seek_bit(4);
        assert_eq!(bytes.read(1).unwrap(),vec![0b00111111]);
        bytes.seek_bit(4);
        assert_eq!(bytes.read(2).unwrap(),vec![0b00111111,0b00001111]);
        assert!(bytes.read(3).is_err())
    }
    #[test]
    fn test_bytes_read_bit() {
        let mut bytes = Bytes::new_from_vec(vec![0xF0,0b01011100]);
        assert_eq!(bytes.read_bit(4).unwrap(),vec![true,true,true,true]);
        assert_eq!(bytes.read_bit(2).unwrap(),vec![false,false]);
        bytes.seek_bit(4);
        assert_eq!(bytes.read_bit(2).unwrap(),vec![false,false]);
        bytes.seek_bit(7);
        assert_eq!(bytes.read_bit(3).unwrap(),vec![false,false,true]);
        assert!(bytes.read_bit(7).is_err());
        bytes.seek(1);
        assert_eq!(bytes.read_bit(7).unwrap(),vec![false,true,false,true,true,true,false])
    }
    #[test]
    fn test_bytes_seek_bit() {
        let mut bytes = Bytes::new_from_vec(vec![4]);
        bytes.seek_bit(4);
        bytes.seek_bit(9);
        assert_eq!(bytes.tell_bit(),9);
    }
    #[test]
    fn test_bytes_set_byte() {
        let mut bytes = Bytes::new_from_vec(vec![4,2]);
        bytes.set_byte(1,10).unwrap();
        assert_eq!(bytes.get_byte(1).unwrap(), 10);
        assert!(bytes.set_byte(2,10).is_err());
        assert!(bytes.set_byte(40,10).is_err());
    }
    #[test]
    fn test_bytes_add_a_byte() {
        let mut bytes = Bytes::new_from_vec(vec![1]);
        bytes.add_a_byte(5).unwrap();
        assert_eq!(bytes.get_byte(1).unwrap(), 5);
    }

}
