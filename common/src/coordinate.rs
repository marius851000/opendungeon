use std::ops::Add;
use std::convert::TryFrom;

/// represent a coordinate
///
/// in dungeongeneration, the zero point is at the lower-left point of the grid
#[derive(Debug, Copy, Clone, PartialEq)]
pub struct Coordinate {
    /// the x coordinate, that grow as it goes up
    pub x: usize,
    /// the y coordinate, that grow as it goes right
    pub y: usize,
}

//TODO: use macro for implementation

impl Add for Coordinate {
    type Output = Self;

    /// addition between two Coordinate (do not check integer overflow)
    ///
    /// Note: if you want to do substraction, consider transform into a SCoordinate first.
    ///
    /// # Examples
    ///
    /// ```
    /// use opendungeon_common::Coordinate;
    /// let coord1 = Coordinate{x:1,y:2};
    /// let coord2 = Coordinate{x:4,y:2};
    /// assert_eq!(Coordinate{x:5,y:4}, coord1 + coord2)
    /// ```
    fn add(self, other: Self) -> Self{
        Coordinate{
            x: self.x + other.x,
            y: self.y + other.y,
        }
    }
}

impl TryFrom<SCoordinate> for Coordinate {
    type Error = &'static str;
    /// try to transform [`SCoordinate`] to [`Coordinate`]. Return the error as string if it can't, due to loss with the transformation from deleting less.
    ///
    /// # Examples
    ///
    /// ```
    /// use opendungeon_common::{Coordinate, SCoordinate};
    /// use std::convert::TryFrom;
    ///
    /// let scoord = SCoordinate{x:10, y:10};
    /// let coord = Coordinate::try_from(scoord).unwrap();
    /// assert_eq!(Coordinate{x:10, y:10}, coord);
    ///
    /// let scoord2 = SCoordinate{x:-10, y:10};
    /// assert!(Coordinate::try_from(scoord2).is_err());
    /// ```
    fn try_from(value: SCoordinate) -> Result<Self, Self::Error> {
        let x = match usize::try_from(value.x) {
            Err(_) => return Err("Impossible to transform value.x to usize"),
            Ok(value) => value,
        };
        let y = match usize::try_from(value.y) {
            Err(_) => return Err("Impossible to transform value.y to usize"),
            Ok(value) => value,
        };
        Ok(Coordinate{x, y})
    }
}

/// A signed [`Coordinate`]
#[derive(Debug, Copy, Clone, PartialEq)]
pub struct SCoordinate {
    /// the x coordinate
    pub x: isize,
    /// the y coordinate
    pub y: isize,
}

impl Add for SCoordinate {
    type Output = Self;

    /// addition between two SCoordinate (do not check integer overflow)
    ///
    /// # Examples
    ///
    /// ```
    /// use opendungeon_common::SCoordinate;
    /// let coord1 = SCoordinate{x:1,y:-1};
    /// let coord2 = SCoordinate{x:-1,y:2};
    /// assert_eq!(SCoordinate{x:0,y:1}, coord1 + coord2)
    /// ```
    fn add(self, other: Self) -> Self{
        SCoordinate{
            x: self.x + other.x,
            y: self.y + other.y,
        }
    }
}

impl TryFrom<Coordinate> for SCoordinate {
    type Error = &'static str;
    /// try to transform [`Coordinate`] to [`SCoordinate`]. Return the error as string if it can't, due to loss with the transformation from usize to isize
    ///
    /// # Examples
    ///
    /// ```
    /// use opendungeon_common::{Coordinate, SCoordinate};
    /// use std::convert::TryFrom;
    ///
    /// let coord = Coordinate{x:10, y:10};
    /// let scoord = SCoordinate::try_from(coord).unwrap();
    /// assert_eq!(SCoordinate{x:10, y:10}, scoord);
    ///
    /// let coord2 = Coordinate{x:std::usize::MAX, y:10};
    /// assert!(SCoordinate::try_from(coord2).is_err());
    /// ```
    fn try_from(value: Coordinate) -> Result<Self, Self::Error> {
        let x = match isize::try_from(value.x) {
            Err(_) => return Err("Impossible to transform value.x to isize"),
            Ok(value) => value,
        };
        let y = match isize::try_from(value.y) {
            Err(_) => return Err("Impossible to transform value.y to isize"),
            Ok(value) => value,
        };
        Ok(SCoordinate{x, y})
    }
}
