#[macro_use]
extern crate log;

mod side;
pub use crate::side::Side;
mod coordinate;
pub use crate::coordinate::Coordinate;
pub use crate::coordinate::SCoordinate;
pub mod pathfinding;
mod bytes;
pub use crate::bytes::Bytes;
mod rabytes;
pub use crate::rabytes::RABytes;


#[test]
fn test_get_bit() {
    assert_eq!(get_bit(0b10000000,0).unwrap(), true);
    assert_eq!(get_bit(0b00000000,0).unwrap(), false);
    assert_eq!(get_bit(0b00100000,2).unwrap(), true);
    assert_eq!(get_bit(0b00000001,7).unwrap(), true);
    assert_eq!(get_bit(0b00000000,7).unwrap(), false);
    assert!(get_bit(0,8).is_none());
}

/// get the bit number id in the byte
///
/// return None if outside of bound
pub fn get_bit(byte: u8, id: usize) -> Option<bool> {
    if id < 8 {
        Some((byte >> (7-id) << 7) >= 1)
    } else {
        None
    }
}

/// get the bit number id in the byte
///
/// return None if outside of bound
pub fn get_bit_u16(byte: u16, id: usize) -> Option<bool> {
    if id < 8 {
        Some((byte >> (15-id) << 15) >= 1)
    } else {
        None
    }
}
