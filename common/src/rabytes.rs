use std::string::String;
use std::{fs::File, io::Write};
use std::path::PathBuf;
use crate::Bytes;
// rabytes for random access bytes.


macro_rules! read_array_x {
    ($func_name:ident, $x:literal) => {
        fn $func_name(&mut self) -> Result<[u8; $x], &'static str> {
            let bytes = match self.read($x) {
                Ok(o) => o,
                Err(_) => return Err("Unable to read $x bytes, to transform them in an array of $x bytes"),
            };
            let mut array = [0; $x];
            array.copy_from_slice(bytes.as_slice());
            Ok(array)
        }
    };
}


macro_rules! read_x {
    ($func_name:ident, $get_slice:ident, $type: ty, $transform_func:ident) =>
    {
        fn $func_name(&mut self) -> Result<$type, &'static str> {
            let bytes = self.$get_slice()?;
            Ok(<$type>::$transform_func(bytes))
        }
    }
}

macro_rules! write_x {
    ($func_name:ident, $type: ty, $transform_func:ident) =>
    {
        fn $func_name(&mut self, value: $type) -> Result<(), &'static str> {
            let bytes = value.$transform_func();
            self.write_vec(bytes.to_vec())?;
            Ok(())
        }
    }
}

macro_rules! read_x_l8_bit_le {
    ($func_name:ident, $lenght:literal) =>
    {
        /// return a u8 representing a $lenght bit number
        fn $func_name(&mut self) -> Result<u8, &'static str> {
            let bits = self.read_bit($lenght)?;
            let mut result: u8 = 0;
            for bit in bits {
                result <<= 1;
                if bit {
                    result += 1;
                }
            }
            Ok(result)
        }
    }
}

macro_rules! read_x_u8_bit_le {
    ($func_name:ident, $lenght:literal, $return:ty) =>
    {
        /// return a u8 representing a $lenght bit number
        fn $func_name(&mut self) -> Result<$return, &'static str> {
            let num_full_byte: u16 = $lenght/8;
            let remaining_bit: u16 = $lenght%8;
            let mut result: $return = 0;
            for bit in 0..remaining_bit {
                let actual_bit = (self.read_u1_le()? as $return) << (num_full_byte*8+remaining_bit-1-bit);
                result += actual_bit;
            }
            for repeat in 0..num_full_byte {
                let actual_byte = (self.read_u8_le()? as $return) << repeat;
                result += actual_byte
            }
            Ok(result)
        }
    }
}

#[allow(clippy::len_without_is_empty)]
pub trait RABytes {
    /// read all the content of self, which doesn't increment the pointer, and return it as a vector
    ///
    /// # Examples
    ///
    /// ```
    /// use opendungeon_common::{Bytes, RABytes};
    /// let b = Bytes::new_from_vec(b"hello".to_vec());
    /// assert_eq!(b.read_all_vec(), b"hello".to_vec());
    /// assert_eq!(b.tell(), 0);
    /// ```
    fn read_all_vec(&self) -> Vec<u8> {
        let mut result = Vec::new();
        for num in 0..self.len() {
            result.push(self.get_byte(num).unwrap());
        }
        result
    }

    /// return the total number of bytes stored in this object
    fn len(&self) -> u64;

    /// return the lenght in bit
    fn len_bit(&self) -> u128 {
        u128::from(self.len()*8)
    }

    /// return the position of the pointer, in bit
    fn tell_bit(&self) -> u128;

    /// return the position of the pointer, in bytes (can lead to error if there are a bit swift).
    fn tell(&self) -> u64 {
        (self.tell_bit()/8) as u64
    }

    fn is_aligned(&self) -> bool {
        match self.tell_bit()%8 {
            0 => true,
            _ => false,
        }
    }

    /// set the pointer to this value (in bit)
    fn seek_bit(&mut self, position: u128);

    /// set the pointer to this value (in bytes)
    fn seek(&mut self, position: u64) {
        self.seek_bit(u128::from(position*8))
    }

    /// set the pointer at the start of the file. Guaranted to succedd
    fn seek_0(&mut self) {
        self.seek(0);
    }

    /// read a number of bytes, and return it as u8.
    fn read(&mut self, lenght: u64) -> Result<Vec<u8>, &'static str> {
        if self.tell() + lenght > self.len() {
            Err("impossible to read enought bytes")
        } else {
            let mut result = Vec::new();
            for position in self.tell()..self.tell()+lenght {
                result.push(self.get_byte(position).unwrap());
            }
            self.seek(self.tell() + lenght);
            Ok(result)
        }
    }

    /// read a number of bit, and return them as bool
    //TODO: default implementation with get_byte
    fn read_bit(&mut self, lenght: u128) -> Result<Vec<bool>, &'static str>;

    /// return the byte a the pos 'pos'
    fn get_byte(&self, pos: u64) -> Result<u8, &'static str>;

    /// create another [`Bytes`] object, copying a part of this [`Bytes`]
    ///
    /// # Examples
    ///
    /// ```
    /// use opendungeon_common::{Bytes, RABytes};
    /// let mut bytes = Bytes::new_from_vec(b"hello world!".to_vec());
    /// assert_eq!(bytes.create_sub_file(6,5).unwrap().read(5).unwrap(),b"world");
    /// ```
    fn create_sub_file(&self, start_offset: u64, lenght: u64) -> Result<Bytes, &'static str> {
        if lenght+start_offset > self.len() {
            return Err("impossible to create a subfile, as it will overflow it's parent!!!");
        }
        let mut result = Bytes::new();
        for byte in start_offset..start_offset+lenght {
            result.add_a_byte(self.get_byte(byte)?)?;
        }
        Ok(result)
    }

    fn set_byte(&mut self, pos: u64, value: u8) -> Result<(), &'static str>;
    fn add_a_byte(&mut self, value: u8) -> Result<(), &'static str>;

    fn push(&mut self, data: Vec<u8>) -> Result<(), &'static str> {
        for byte in data {
            self.add_a_byte(byte)?;
        }
        Ok(())
    }

    fn display_hex(&self) {
        for byte_id in 0..self.len() {
            let byte = self.get_byte(byte_id).unwrap();
            for hex_num in [(byte >> 4), (byte << 4 >> 4)].iter() {
                print!("{}", match hex_num {
                    0 => "0",
                    1 => "1",
                    2 => "2",
                    3 => "3",
                    4 => "4",
                    5 => "5",
                    6 => "6",
                    7 => "7",
                    8 => "8",
                    9 => "9",
                    10 => "a",
                    11 => "b",
                    12 => "c",
                    13 => "d",
                    14 => "e",
                    15 => "f",
                    _ => panic!(),
                })
            }
        }
        println!();
    }


    read_array_x!(read_array_1, 1);
    read_array_x!(read_array_2, 2);
    read_array_x!(read_array_4, 4);
    read_array_x!(read_array_8, 8);
    read_array_x!(read_array_16, 16);

    read_x_l8_bit_le!(read_u8_le, 8);
    read_x!(read_u16_le, read_array_2, u16, from_le_bytes);
    read_x!(read_u32_le, read_array_4, u32, from_le_bytes);
    read_x!(read_u64_le, read_array_8, u64, from_le_bytes);
    read_x!(read_u128_le, read_array_16, u128, from_le_bytes);

    read_x!(read_i8_le, read_array_1, i8, from_le_bytes);
    read_x!(read_i16_le, read_array_2, i16, from_le_bytes);
    read_x!(read_i32_le, read_array_4, i32, from_le_bytes);
    read_x!(read_i64_le, read_array_8, i64, from_le_bytes);
    read_x!(read_i128_le, read_array_16, i128, from_le_bytes);

    read_x_l8_bit_le!(read_u1_le, 1);
    read_x_l8_bit_le!(read_u2_le, 2);
    read_x_l8_bit_le!(read_u3_le, 3);
    read_x_l8_bit_le!(read_u4_le, 4);
    read_x_l8_bit_le!(read_u5_le, 5);
    read_x_l8_bit_le!(read_u6_le, 6);
    read_x_l8_bit_le!(read_u7_le, 7);

    read_x_u8_bit_le!(read_u9_le, 9, u16);
    read_x_u8_bit_le!(read_u10_le, 10, u16);


    read_x!(read_u16_be, read_array_2, u16, from_be_bytes);
    read_x!(read_u32_be, read_array_4, u32, from_be_bytes);
    read_x!(read_u64_be, read_array_8, u64, from_be_bytes);
    read_x!(read_u128_be, read_array_16, u128, from_be_bytes);

    /// read a string in ascii from the given lenght and increase the cursor
    ///
    /// # Examples :
    ///
    /// ```
    /// use opendungeon_common::{Bytes, RABytes};
    /// let mut bytes = Bytes::new_from_vec(b"helloworld".to_vec());
    /// assert_eq!(bytes.read_string_ascii_lenght(5).unwrap(), String::from("hello"));
    /// ```
    fn read_string_ascii_lenght(&mut self, lenght: u64) -> Result<String, &'static str> {
        let mut result = String::new();
        for _ in 0..lenght {
            let char = self.read_u8_le()? as char;
            result.push(char);
        }
        Ok(result)
    }

    fn read_string_ascii_null_terminated(&mut self) -> Result<String, &'static str> {
        let mut result = String::new();
        loop {
            let chara = self.read_u8_le()?;
            if chara == 0 {
                return Ok(result);
            };
            result.push(chara as char);
        };
    }


    /// write an u8 at the pointer. If the pointer is a just after the end of the file, create a new byte
    /// It also increment the counter by 1.
    /// # Examples
    ///
    /// ```
    /// use opendungeon_common::{Bytes, RABytes};
    /// let mut bytes = Bytes::new_from_vec(vec![1,2]);
    /// bytes.seek(1);
    /// bytes.write_u8_le(10).unwrap();
    /// assert_eq!(bytes.read_all_vec(), [1,10]);
    /// bytes.write_u8_le(5).unwrap();
    /// assert_eq!(bytes.read_all_vec(), [1,10,5]);
    /// ```
    ///
    /// ```
    /// use opendungeon_common::{Bytes, RABytes};
    /// let mut bytes = Bytes::new_from_vec(vec![0,1,2,3]);
    /// bytes.seek(10);
    /// match bytes.write_u8_le(10) {Err(_)=>(()),Ok(_)=>panic!()};
    /// ```
    fn write_u8_le(&mut self, value: u8) -> Result<(), &'static str> {
        if self.len() < self.tell() {
            return Err("impossible to add a byte outside of the file");
        };
        if self.tell() == self.len() {
            self.add_a_byte(value)?;
        } else {
            self.set_byte(self.tell(), value)?;
        };
        self.seek(self.tell()+1);
        Ok(())
    }

    fn write_vec(&mut self, content: Vec<u8>) -> Result<(), &'static str> {
        for l in content {
            self.write_u8_le(l)?;
        }
        Ok(())
    }

    fn write(&mut self, content: Vec<u8>) -> Result<(), &'static str> {
        self.write_vec(content)
    }

    /// copy the content of a [RABytes] to this [RABytes]
    fn write_bytes<T: RABytes>(&mut self, content: &T) -> Result<(), &'static str> {
        for pos in 0..content.len() {
            self.write_u8_le(content.get_byte(pos)?)?;
        };
        Ok(())
    }

    /// write a null terminated ascii string to this [RABytes]
    fn write_ascii_string_null_terminated(&mut self, str: &String) -> Result<(), &'static str>{
        for char in str.chars() {
            self.write_u8_le(char as u8)?;
        }
        self.write_u8_le(0)?;
        Ok(())
    }

    write_x!(write_u16_le, u16, to_le_bytes);
    write_x!(write_u32_le, u32, to_le_bytes);
    write_x!(write_u64_le, u64, to_le_bytes);
    write_x!(write_u128_le, u128, to_le_bytes);

    write_x!(write_u16_be, u16, to_be_bytes);
    write_x!(write_u32_be, u32, to_be_bytes);
    write_x!(write_u64_be, u64, to_be_bytes);
    write_x!(write_u128_be, u128, to_be_bytes);

    write_x!(write_i16_le, i16, to_le_bytes);
    write_x!(write_i32_le, i32, to_le_bytes);
    write_x!(write_i64_le, i64, to_le_bytes);
    write_x!(write_i128_le, i128, to_le_bytes);

    /// test if two [RABytes] object are the same
    fn equal<T: RABytes>(&self, other: &T) -> Result<bool, &'static str> {
        if self.len() != other.len() {
            return Ok(false)
        };
        for byte_id in 0..self.len() {
            if self.get_byte(byte_id)? != other.get_byte(byte_id)? {
                return Ok(false)
            };
        };
        Ok(true)
    }

    /// write the [RABytes] object to the file designed by input_path
    ///
    /// return an error if there is an I/O error
    fn write_into_file(&self, input_path: &PathBuf) -> Result<(), std::io::Error> {
        let mut out_file = File::create(input_path)?;
        out_file.write_all(&self.read_all_vec())?;
        Ok(())
    }

    fn write_padding(&mut self, padding: u8, multiple: u64) -> Result<(), &'static str> {
        while self.tell()%multiple != 0 {
            self.write_u8_le(padding)?;
        };
        Ok(())
    }
}

#[test]
fn test_rabytes_equal() {
    let first = Bytes::new_from_vec(vec![0x01, 0x11, 0x65]);
    let second = Bytes::new_from_vec(vec![0x01, 0x11, 0x65]);
    assert_eq!(first.equal(&second).unwrap(), true);
    let third = Bytes::new_from_vec(vec![0x01, 0x11, 0x65, 0x65]);
    assert_eq!(first.equal(&second).unwrap(), true);
    assert_eq!(second.equal(&first).unwrap(), true);
    assert_eq!(first.equal(&third).unwrap(), false);
    let fourth = Bytes::new_from_vec(vec![0x01, 0x11, 0x66]);
    assert_eq!(first.equal(&fourth).unwrap(), false);

}

#[test]
fn test_read_u10() {
    let mut b = Bytes::new_from_vec(vec![0x0F,0xF0]);
    b.seek_bit(6);
    let value = b.read_u10_le().unwrap();
    assert_eq!(value, 0x03F0);
}

#[test]
fn test_write_padding() {
    let mut first = Bytes::new_from_vec(vec![0,0]);
    first.seek(2);
    first.write_padding(0xFF, 4).unwrap();
    println!("{:?}", first);
    assert_eq!(first.equal(&Bytes::new_from_vec(vec![0,0,0xFF,0xFF])).unwrap(), true);
    let mut second = Bytes::new_from_vec(vec![0,0]);
    second.seek(2);
    second.write_padding(0xAA, 2).unwrap();
    assert_eq!(second.equal(&Bytes::new_from_vec(vec![0,0])).unwrap(), true);
}
