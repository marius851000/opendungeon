#[macro_use]
extern crate log;
extern crate env_logger;

use opendungeon_dungeongeneration::random_generation::RandomGeneration;
use opendungeon_dungeongeneration::generator::Generate;

fn main() {
    env_logger::init();

    info!("logger started");

    /*info!("testing by generating a lot of dungeon");
    for _ in 0..10000 {
        RandomGeneration::new(50, 50).generate();//.display();
    }*/
    info!("producing good looking dungeon");
    for _ in 0..1 {
        RandomGeneration::new(96, 96).generate().display();
        //println!("------------------------------");
    };
    info!("program finished");
}
