//! contain a generator for a Floor generated with the random method
extern crate rand;
extern crate log;

use crate::place_obstacle::place_obstacle;
use crate::generator::Generate;
use crate::room_grid::{RoomGrid, RoomType};
use dungeon::floor;
use dungeon::tile::FloorType;
use rand::prelude::*;
use log::Level;

/// contain setting about the random generation
pub struct RandomGeneration {
    /// horizontal number of tile
    pub xsize: usize,
    /// vertical number of tile
    pub ysize: usize,
    /// the maximum room size (only applicable for a [`RoomType::Normal`]\(1\))
    pub average_room_size: usize,
    /// the random seed. Currently unused
    pub seed: usize,
    /// the luck (over 1) there is [`RoomType::Wide`] or [`RoomType::High`] rooms
    pub luck_of_big_room: f64,
    /// the luck (over 1) that, if it can generate two [`RoomType::Wide`] or [`RoomType::High`] rooms, it generate only one of these
    pub luck_of_reduce_2_big_in_1: f64,
    /// the luck (over 1) to generate a [`RoomType::Normal`]\(2\) rather than a [`RoomType::Normal`]\(1\). Note that [`RoomType::Normal`]\(1\) will spawn rather than [`RoomType::Normal`]\(2\) if they can't be placed.
    pub luck_of_room_of_two: f64,
    /// the minimal space between rooms
    pub extra_room_space: usize,
    /// the obstacle [`FloorType`] that will replace some wall
    pub obstacle_type: FloorType,
    /// the pourcentage of chance for a wall to be an obstacle (over 1)
    pub obstacle_chance: f64,
    /// the proportion of obstacle source (over 1)
    pub obstacle_source_proportion: f64,
}

#[test]
fn test_random_generation () {
    let _ = RandomGeneration::new(60, 60).generate();
}

impl Generate for RandomGeneration {
    /// randomly generate the dungeon, based on the algoritm described in [dungeon-generation.md] file (not exacly identical)
    ///
    /// # Examples
    ///
    /// ```
    /// use opendungeon_dungeongeneration::{random_generation::RandomGeneration, generator::Generate};
    /// let floor = RandomGeneration::new(40,40).generate();
    /// ```
    fn generate(&self) -> floor::Floor {
        //TODO: use the seed
        debug!("started the generation of a floor");

        let mut rng = thread_rng();
        let mut level = floor::FloorBuilder::new(self.xsize, self.ysize).initialize();

        let max_room_number_x = (self.xsize-self.extra_room_space) / (self.average_room_size + self.extra_room_space);
        let max_room_number_y = (self.ysize-self.extra_room_space) / (self.average_room_size + self.extra_room_space);
        let room_in_less = rng.gen_range(0, 4);
        let mut grid = RoomGrid::new(max_room_number_x, max_room_number_y);

        trace!("placing wide/high room...");
        grid.place_big_room(&mut rng, self.luck_of_big_room, self.luck_of_reduce_2_big_in_1);

        trace!("filling the remaining space with normal room...");
        let mut can_still_place_two = true;
        while grid.remaining_space() > room_in_less {
            if grid.place_room_random(RoomType::Normal(
                if can_still_place_two {
                    if rng.gen_bool(self.luck_of_room_of_two) {2} else {1}
                } else {1}
            ), &mut rng) == None {
                can_still_place_two = false;
            };
        };

        if log_enabled!(Level::Trace) {
            trace!("displaying the generated room grid ...");
            grid.display();
            trace!("grid displayed");
        };

        trace!("generating position of room and doors...");
        let number_of_rooms = grid.rooms.len();
        for room in &mut grid.rooms {
            room.generate(self.average_room_size, self.extra_room_space, &mut rng);
            if number_of_rooms != 1 {
                room.add_doors_randomly(&mut rng);
            };
        }

        // placing room on the grid
        for room in &grid.rooms {
            room.place(&mut level);
        };

        trace!("generating doors links...");

        grid.generate_door_link(&mut rng);

        trace!("placing corridors...");

        grid.place_corridor(&mut level);

        trace!("transfering rooms ownership to the floor...");

        level.rooms = Some(Vec::new());

        for room in grid.rooms {
            match room.applied {
                None => error!("found an non-applied room while transfering its ownership to the floor!!! Skipping it, hoping it won't cause a problem."),
                Some(room_applied) => {
                    match &mut level.rooms {
                        None => panic!(),
                        Some(t) => t.push(room_applied),
                    };
                },
            }
        }

        trace!("placing water");

        place_obstacle(&mut level, &mut rng, self.obstacle_type, self.obstacle_chance, self.obstacle_source_proportion);

        trace!("generation of the floor finished.");

        //TODO: water/lava/hole
        level
    }
}

impl RandomGeneration {
    /// create a new [`RandomGeneration`] object
    ///
    /// xsize is the horizontal number of tile
    /// ysize is the vertical number of tile
    pub fn new(xsize: usize, ysize: usize) -> RandomGeneration {
        RandomGeneration {
            xsize,
            ysize,
            average_room_size: 6,
            seed: 0,
            luck_of_big_room: 0.7,
            luck_of_reduce_2_big_in_1: 0.5,
            luck_of_room_of_two: 0.25,
            extra_room_space: 4,
            obstacle_type: FloorType::Water,
            obstacle_chance: 0.07,
            obstacle_source_proportion: 0.10,
        }
    }
}
