use common::pathfinding::get_distances;
use dungeon::tile::{FloorType, Tile};
use dungeon::floor::Floor;
use common::{Side, Coordinate};
use rand::{rngs::ThreadRng, Rng};


const SOURCE_CHANCE_ADJACENT_PATH_BONUS: u64 = 10; //default: 10
const SOURCE_CHANCE_ADJACENT_ROOM_BONUS: u64 = 20; //default: 20
const OBSTACLE_CHANCE_ADJACENT_PATH_BONUS: u64 = 3; // default: 5
const OBSTACLE_CHANCE_ADJACENT_OBSTACLE_BONUS: u64 = 10; //default: 10
const OBSTACLE_CHANCE_ADJACENT_ROOM_BONUS: u64 = 1; //default: 15

fn chance_bonus_distance(dist: usize) -> u64 {
    match dist {
        0 => 5,
        1 => 20,
        2 => 50,
        3 => 10,
        _ => 0,
    }
}

fn get_tile_chance(coord: &Coordinate, floor: &Floor, obstacle_type: &FloorType, distance_from_path: &Vec<Vec<usize>>) -> Option<u64> {
    let x = coord.x;
    let y = coord.y;
    match floor.level[x][y].floor {
        FloorType::Wall => {
            let mut is_adjacent_to_obstacle = false;
            let mut chance: u64 = 0;
            for side in Side::iterator() {
                if !side.is_legal(Coordinate{x,y},floor.xsize, floor.ysize) {
                    continue;
                };
                let actual_coordinate = Coordinate {
                    x: (x as isize + side.x_movement()) as usize,
                    y: (y as isize + side.y_movement()) as usize,
                };
                match match floor.get_tile(&actual_coordinate) {
                        None => {
                            error!("tried to access to a non-valid tile coordinate while placing water!!! ignoring it.");
                            continue;
                        },
                        Some(x) => x
                    }.floor {
                    FloorType::Path => chance += OBSTACLE_CHANCE_ADJACENT_PATH_BONUS,
                    x if x == *obstacle_type => {
                        is_adjacent_to_obstacle = true;
                        chance += OBSTACLE_CHANCE_ADJACENT_OBSTACLE_BONUS;
                    },
                    _ => {},
                };

                if floor.get_room_at(&actual_coordinate).is_some() {
                    chance += OBSTACLE_CHANCE_ADJACENT_ROOM_BONUS;
                };
                let distance = distance_from_path[actual_coordinate.x][actual_coordinate.y];
                chance += chance_bonus_distance(distance);
            }
            // check that the water is adjacent to an obstacle
            if is_adjacent_to_obstacle {
                Some(chance)
            } else {
                Some(0)
            }
        },
        _ => None,
    }
}
fn compute_tile_chance(floor: &Floor, obstacle_type: &FloorType, distance_from_path: &Vec<Vec<usize>>) -> (u64, Vec<Vec<Option<u64>>>) {
    let mut pourcentage_of_chance_of_obstacle_by_tile = Vec::new();
    let mut sum_of_chance = 0;
    for x in 0..floor.xsize {
        pourcentage_of_chance_of_obstacle_by_tile.push(Vec::new());
        for y in 0..floor.ysize {
            let this_tile_chance = get_tile_chance(
                &Coordinate{x, y},
                floor,
                obstacle_type,
                distance_from_path,
            );
            match this_tile_chance {
                Some(chance) => sum_of_chance += chance,
                _ => {},
            }
            pourcentage_of_chance_of_obstacle_by_tile[x].push(this_tile_chance);
        }
    }
    (sum_of_chance, pourcentage_of_chance_of_obstacle_by_tile)
}

/// place obstacle, replacing [`FloorType::Wall`]
///
/// it take into account the proximity of a [`Room`], that increase the chance of placement, of a [`FloorType::Path`], and of another obstacle of the same type.
///
/// obstacle_type is the [`FloorType`] to place
/// obstacle_proportion is the proportion (over 1) of [`FloorType::Wall`] to replace with the obstacle
/// source_proportion is the proportion (over 1) of source. Basically, a greater value should made obstacle more disparate
#[allow(clippy::needless_update)]
#[allow(clippy::needless_range_loop)]
#[allow(clippy::cast_lossless)]
pub fn place_obstacle(floor: &mut Floor, rng: &mut ThreadRng, obstacle_type: FloorType, obstacle_proportion: f64, source_proportion: f64) {
    // get the sum of wall for this floor
    let mut number_of_wall = 0;
    for x in &floor.level {
        for y in x {
            if y.floor == FloorType::Wall {
                number_of_wall += 1;
            };
        }
    }
    let number_of_obstacle = (number_of_wall as f64 * obstacle_proportion).ceil() as usize;
    let number_of_source = (number_of_obstacle as f64 * source_proportion).ceil() as usize;

    trace!("getting distance of floor");
    let mut non_obstacle_tile: Vec<Vec<bool>> = Vec::new();
    for x in 0..floor.xsize {
        non_obstacle_tile.push(Vec::new());
        for y in 0..floor.ysize {
            non_obstacle_tile[x].push(
                match floor.level[x][y].floor {
                    FloorType::Wall => false,
                    _ => true,
                }
            );
        }
    }

    let distance_from_path = get_distances(non_obstacle_tile);

    trace!("placing initial obstacle");
    let mut possible_obstacle_source: Vec<Vec<Option<u64>>> = Vec::new();
    let mut sum_of_chance: u64 = 0;
    for x in 0..floor.xsize {
        possible_obstacle_source.push(Vec::new());
        for y in 0..floor.ysize {
            possible_obstacle_source[x].push(match floor.level[x][y].floor {
                FloorType::Wall => {
                    let mut adjacent_to_path = false;
                    let mut adjacent_to_room = false;
                    for side in Side::iterator() {
                        if !side.is_legal(Coordinate{x,y},floor.xsize, floor.ysize) {
                            continue;
                        };

                        let actual_coordinate = Coordinate {
                            x: (x as isize + side.x_movement()) as usize,
                            y: (y as isize + side.y_movement()) as usize,
                        };

                        let tile = match floor.get_tile(&actual_coordinate) {
                            None => {
                                error!("tried to access to a off grid tile while computing chance for wall to be an obstacle source!!! ignoring it.");
                                continue;
                            },
                            Some(value) => value,
                        };

                        if tile.floor == FloorType::Path {
                            adjacent_to_path = true;
                        };

                        match floor.get_room_at(&actual_coordinate) {
                            None => {},
                            Some(_) => adjacent_to_room = true,
                        }
                    }
                    let chance = if adjacent_to_path {
                            SOURCE_CHANCE_ADJACENT_PATH_BONUS +
                            if adjacent_to_room {
                                SOURCE_CHANCE_ADJACENT_ROOM_BONUS
                            } else {
                                0
                            }
                        } else {
                            0
                        };
                    sum_of_chance += chance;
                    Some(chance)
                },
                _ => None,
            })
        }
    }

    for _ in 0..number_of_source {
        let choice = rng.gen_range(0, sum_of_chance);
        let mut already_passed_chance = 0;
        let mut source = None;
        'search_source_obstacle_tile: for x in 0..floor.xsize {
            for y in 0..floor.ysize {
                match possible_obstacle_source[x][y] {
                    None => continue,
                    Some(chance) => {
                        already_passed_chance += chance;
                        if already_passed_chance > choice {
                            source = Some(Coordinate{x,y});
                            break 'search_source_obstacle_tile;
                        }
                    }
                }
            }
        }
        let coord = match source {
            None => {
                error!("no source found for the choice {} (over a sum of chance of {})!!! skipping it.", choice, sum_of_chance);
                continue;
            },
            Some(x) => x,
        };
        let chance = match possible_obstacle_source[coord.x][coord.y] {
            None => {
                error!("an unselectionable possible obstacle source was selectioned!!! skipping it.");;
                continue;
            },
            Some(x) => x,
        };
        possible_obstacle_source[coord.x][coord.y] = None;
        sum_of_chance -= chance;
        floor.level[coord.x][coord.y] = Tile{ floor: obstacle_type, .. Tile::new()};
    }

    trace!("placing the rest of the obstacles");



    let (mut sum_of_chance, mut pourcentage_of_chance_of_obstacle_by_tile) = compute_tile_chance(&floor, &obstacle_type, &distance_from_path);
    'place_obstacle: for _ in 0..number_of_obstacle-number_of_source {
        // choose the tile
        let choice = rng.gen_range(0, sum_of_chance);

        let mut already_passed_chance = 0;
        let mut source = None;
        'search_chosen_tile: for x in 0..floor.xsize {
            for y in 0..floor.ysize {
                match pourcentage_of_chance_of_obstacle_by_tile[x][y] {
                    None => continue,
                    Some(chance) => {
                        already_passed_chance += chance;
                        if already_passed_chance > choice {
                            source = Some(Coordinate{x,y});
                            break 'search_chosen_tile;
                        }
                    }
                }
            }
        }
        let coord = match source {
            None => {
                error!("no source found for the choice {} (for a sum of chance of {})!!! skipping it.", choice, sum_of_chance);
                continue;
            },
            Some(x) => x,
        };

        // place the obstacle tile
        floor.level[coord.x][coord.y] = Tile{ floor: obstacle_type, .. Tile::new()};

        // update chances
        let actual_chance = match pourcentage_of_chance_of_obstacle_by_tile[coord.x][coord.y] {
            Some(value) => value,
            None => {
                error!("the selected tile have no chance of being selected althrought it is selected!!! recalculating the tile elimination layer instead.");
                let temp = compute_tile_chance(&floor, &obstacle_type, &distance_from_path);
                sum_of_chance = temp.0;
                pourcentage_of_chance_of_obstacle_by_tile = temp.1;
                continue 'place_obstacle;
            },
        };

        pourcentage_of_chance_of_obstacle_by_tile[coord.x][coord.y] = None;
        sum_of_chance -= actual_chance;

        for side in Side::iterator() {
            if !side.is_legal(Coordinate{x:coord.x,y:coord.y},floor.xsize, floor.ysize) {
                continue;
            };
            let actual_coordinate = Coordinate {
                x: (coord.x as isize + side.x_movement()) as usize,
                y: (coord.y as isize + side.y_movement()) as usize,
            };
            let old_tile_chance = match pourcentage_of_chance_of_obstacle_by_tile[actual_coordinate.x][actual_coordinate.y] {
                Some(value) => value,
                None => continue,
            };
            let new_tile_chance = match get_tile_chance(&actual_coordinate, &floor, &obstacle_type, &distance_from_path) {
                Some(value) => value,
                None => 0,
            };
            pourcentage_of_chance_of_obstacle_by_tile[actual_coordinate.x][actual_coordinate.y] = match new_tile_chance {0=>None, x=>Some(x)};
            let difference_of_chance: i64 = (new_tile_chance as i64) - (old_tile_chance as i64);
            sum_of_chance = ((sum_of_chance as i64) + difference_of_chance) as u64;
        }

        if cfg!(debug_assertions) { // check that value are correcly updated
            let temp = compute_tile_chance(&floor, &obstacle_type, &distance_from_path);

            assert_eq!(sum_of_chance, temp.0);
            assert_eq!(pourcentage_of_chance_of_obstacle_by_tile, temp.1);
        };
    }

    // not used anymore, are intial water is adjacent, and other water is adjacent to water
    /* trace!("removing unacessible obstacle");
    use common::pathfinding::test_accessibility;
    let mut grid: Vec<Vec<bool>> = Vec::new();
    for x in 0..floor.xsize {
        grid.push(Vec::new());
        for y in 0..floor.ysize {
            grid[x].push(
                match floor.level[x][y].floor {
                    FloorType::Wall => false,
                    _ => true,
                }
            );
        }
    }

    let rooms = match &floor.rooms {
        None => {
            error!("error while trying to unpack the rooms for eliminating uninterrising obstacle!!! returning");
            return;
        },
        Some(x) => x,
    };

    if rooms.len() == 0 {
        error!("error while trying to find an accessible room for eliminiting uninterrising obstacle, no rooms where found !!! returning.");
        return;
    };

    let start = rooms[0].coord + Coordinate{x:1,y:1};

    let dead_grid = test_accessibility(&grid, &start);

    for x in 0..floor.xsize {
        for y in 0..floor.ysize {
            if dead_grid[x][y] == false {
                if floor.level[x][y].floor == obstacle_type {
                    floor.level[x][y].floor = FloorType::Wall;
                }
            };
        }
    } */
}
