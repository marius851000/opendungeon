# dungeongeneration
This is an implementation of a random dungeon generation, based on an algoritm of stunthacks, itself based on the level generation of the mystery dungeon serie.

See main.rs for an example of usage. You can also use cargo doc to generate documentation.

Also, please note that it is my first program in rust.

## Speed
This algoritm is rather quick (take actually 0,046s on my computer to generate a 96x96 dungeon in release mode), and 0,003 s by level with a size of 50x50, also in release mode (that is less than a frame !)). BTW, it took 35,86s to generate an insanly big room of 150x1000.

#TODO:
tests
