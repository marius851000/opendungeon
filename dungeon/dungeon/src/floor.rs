//! contain Floor and related function, to store a floor of a dungeon.
use crate::tile::{FloorType, Tile};
use crate::room::Room;
use common::{Coordinate};
use std::io::Write;
use termcolor::{Color, ColorChoice, ColorSpec, StandardStream, WriteColor};
/// Contain floor data ( a level of a dungeon )
/// The zero coordinate are at the down-left
#[derive(Debug)]
pub struct Floor {
    /// the horizontal number of tile of this dungeon
    pub xsize: usize,
    /// the vertical number of tile of this dungeon
    pub ysize: usize,
    /// the first key is x and the second is y
    pub level: Vec<Vec<Tile>>,
    /// the list of [`Room`] this floor contain, is None if uninitialized
    pub rooms: Option<Vec<Room>>,
}

impl Floor {
    /// display the floor in the terminal
    ///
    /// \# are wall, ' ' (space) for floor, ~ for water, = for lava and . for hole
    /// The output is dispayed using color if possible
    ////
    /// # Examples
    ///
    /// ```
    /// use opendungeon_dungeon::floor::FloorBuilder;
    /// use opendungeon_dungeon::tile::FloorType;
    /// let mut floor = FloorBuilder::new(10, 10).initialize();
    /// floor.level[1][1].floor = FloorType::Path;
    /// floor.display()
    /// ```
    pub fn display(&self) {
        let mut stdout = StandardStream::stdout(ColorChoice::Auto);
        for y_reversed in 0..self.ysize {
            let y  = self.ysize-y_reversed-1;
            for x in 0..self.xsize {
                match stdout.set_color(ColorSpec::new().set_fg(Some(match &self.level[x][y].floor {
                    FloorType::Water => Color::Blue,
                    FloorType::Lava => Color::Red,
                    FloorType::Path => Color::Black,
                    FloorType::Hole => Color::Green,
                    FloorType::Wall => Color::White,
                }))) {
                    Ok(_) => (),
                    Err(err) => error!("impossible to change the color of a tile to display the grid: {:?} !!!", err),
                };
                match write!(&mut stdout, "{}", match &self.level[x][y].floor {
                    FloorType::Wall => "#",
                    FloorType::Path => " ",
                    FloorType::Water => "~",
                    FloorType::Lava => "=",
                    FloorType::Hole => "."
                }) {
                    Ok(_) => (),
                    Err(err) => error!("impossible to display a tile of the grid, with the error {:?} !!!", err),
                };
            }
            match writeln!(&mut stdout, "") {
                Ok(_) => (),
                Err(err) => error!("error while jumping a new line in the displaying of a grid: {:?} !!!", err),
            };
        }
    }
    /// return Some(&Tile) if the tile at the coordinate exist, None otherwise
    pub fn get_tile(&self, coord: &Coordinate) -> Option<&Tile> {
        if coord.x >= self.xsize || coord.y >= self.ysize {
            return None;
        }
        return Some(&self.level[coord.x][coord.y]);
    }
    /// return Some(&Room) if the coordinate correspond to a room, None otherwise
    pub fn get_room_at(&self, coord: &Coordinate) -> Option<&Room> {
        match &self.rooms {
            None => {
                error!("trying to get a room in the level while there are not uninitialized!!! Returning None.");
                return None;
            },
            Some(rooms) => {
                for room in rooms {
                    if coord.x >= room.coord.x && coord.y >= room.coord.y && coord.x < room.coord.x + room.xsize && coord.y < room.coord.y + room.ysize {
                        return Some(room);
                    };
                }
                return None;
            }
        }
    }
}

/// allow to easily construct a 'Floor'
///
/// # Examples
///
/// ```
/// use opendungeon_dungeon::floor::FloorBuilder;
/// let mut a = FloorBuilder::new(28, 28).initialize();
/// ```
#[derive(Debug, Clone)]
pub struct FloorBuilder {
    pub xsize: usize,
    pub ysize: usize,
}

impl FloorBuilder {
    /// create a new [`FloorBuilder`]
    ///
    /// xsize is the number of horizontal tile
    /// ysize is the number of vertical tile
    pub fn new(xsize: usize, ysize: usize) -> FloorBuilder {
        FloorBuilder { xsize: xsize, ysize: ysize }
    }

    /// generate a [`Floor`] object
    pub fn initialize(&self) -> Floor {
        Floor {
            xsize: self.xsize,
            ysize: self.ysize,
            level: vec![vec![Tile::new(); self.ysize]; self.xsize],
            rooms: None,
        }
    }
}
