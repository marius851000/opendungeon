use common::Coordinate;
/// store data about a room in a dungeon
#[derive(Debug, Copy, Clone)]
pub struct Room {
    //TODO: use Coordinate instead
    /// the coordinate of the [`Room`]
    pub coord: Coordinate,
    /// the horizontal size of the room
    pub xsize: usize,
    /// the vertical size of the room
    pub ysize: usize,
}
