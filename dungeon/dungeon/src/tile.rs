#[derive(Debug, Copy, Clone, PartialEq)]
/// all possible type of floor
pub enum FloorType {
    /// Wall, unpassable, except maybe with a special capacity
    Wall,
    /// Empty space, can be passed by every things
    Path,
    /// Water, access whitelisted
    Water,
    /// Lava, access whitelisted
    Lava,
    /// Hole, only things that can fly can go over this
    Hole,
}

#[derive(Debug, Clone)]
/// contain a single tile
pub struct Tile {
    /// the type of floor this tile have
    pub floor: FloorType,
}

impl Tile {
    /// create a new tile
    ///
    /// The default floor is a wall
    ///
    /// # Examples
    ///
    /// ```
    /// use opendungeon_dungeon::tile::Tile;
    /// let tile = Tile::new();
    /// ```
    pub fn new() -> Tile {
        Tile{floor: FloorType::Wall}
    }

    /// set the floor of this tile
    pub fn set_floor(&mut self, floor: FloorType) {
        self.floor = floor;
    }
}
