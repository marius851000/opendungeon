#[macro_use]
extern crate log;
#[macro_use]
extern crate error_chain;
extern crate opendungeon_rom_wan;

pub mod errors {
    use sdl2::{video::WindowBuildError, IntegerOrSdlError, render::TextureValueError};
    use opendungeon_rom_wan::errors as wan_errors;
    error_chain!{
        links {
            WanError(wan_errors::Error, wan_errors::ErrorKind);
        }
        foreign_links {
            WindowBuildError(WindowBuildError);
            IntegerOrSdlError(IntegerOrSdlError);
            TextureValueError(TextureValueError);
            IO(std::io::Error);
        }
    }
}

pub mod common;
