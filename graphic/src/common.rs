use crate::errors::*;
use std::thread;
use std::rc::Rc;

// Dynamic loading related stuff

pub enum PreLoadState {
    NotLoading,
    Loading,
    Failed,
    Loaded,
}

pub struct PreLoad<Finished, Intermediary> {
    pub state: PreLoadState,
    handle: Option<thread::JoinHandle<Result<Intermediary>>>,
    result: Option<Rc<Finished>>
}

impl<Finished, Intermediary> PreLoad<Finished, Intermediary> {
    pub fn new_empty() -> PreLoad<Finished, Intermediary> {
        PreLoad {
            state: PreLoadState::NotLoading,
            handle: None,
            result: None,
        }
    }

    pub fn set_status_loading(&mut self, handle: thread::JoinHandle<Result<Intermediary>>) {
        match self.state {
            PreLoadState::NotLoading => self.handle = Some(handle),
            PreLoadState::Loading => {error!("want to preload something which is already loading!!! ignoring it."); return},
            PreLoadState::Failed => {self.handle = Some(handle)},
            PreLoadState::Loaded => {error!("want to preload something which is already fully loaded!!! ignoring it."); return},
        };
        self.state = PreLoadState::Loading;
    }

    pub fn join(&mut self) -> Result<Intermediary> {
        match self.state {
            PreLoadState::NotLoading => bail!("no content is actually loading"),
            PreLoadState::Loading => match self.handle.take() {
                Some(handle) => match handle.join().unwrap().chain_err(|| "error in a thread") { //TODO: don't unwrap
                    Ok(value) => Ok(value),
                    Err(err) => {
                        self.state = PreLoadState::Failed;
                        return Err(err)
                    }
                },
                None => bail!("the value is already taken"),
            },
            PreLoadState::Failed => bail!("the content previously returned a failure"),
            PreLoadState::Loaded => bail!("the content is already loaded"),
        }
    }

    pub fn set_result(&mut self, result: Finished) {
        match self.state {
            PreLoadState::NotLoading => (),
            PreLoadState::Loading => self.handle = None,
            PreLoadState::Failed => (),
            PreLoadState::Loaded => error!("trying to set a content while it was already set!!! overwriting it"),
        };
        self.state = PreLoadState::Loaded;
        self.result = Some(Rc::new(result));
    }

    pub fn get_result(&mut self) -> Result<Rc<Finished>> {
        match self.result.take() { //TODO: quite hacky
            None => bail!("Impossible to get a result, as it not yet computed !"),
            Some(value) => {
                let result = value.clone();
                self.result = Some(value);
                return Ok(result)
            },
        }
    }
}
