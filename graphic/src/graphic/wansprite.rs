use opendungeon_rom_wan::WanImage as WanImg;
use opendungeon_rom_wan::wan::{MetaFrameStore, AnimStore};
use opendungeon_rom_cpack::CPack;
use opendungeon_rom_pkdpx::{decompress_px, is_px};
use crate::graphic::Canvas;
use sdl2::surface::Surface;
use sdl2::pixels::PixelFormatEnum;
use sdl2::render::Canvas as SdlCanvas;
use sdl2::video::Window as SdlWindow;
use sdl2::rect::Rect;
use std::thread;
use opendungeon_graphic::errors::*;
use std::{rc::Rc, sync::Arc};
use opendungeon_graphic::common::{PreLoad, PreLoadState};


pub struct WanStore {
    pack: Arc<CPack>,
    sprites: Vec<PreLoad<WanSprite, WanImg>>,
}

impl WanStore {
    pub fn new(pack: CPack) -> WanStore {
        let mut sprites = Vec::new();
        for _ in 0..pack.len() {
            sprites.push(PreLoad::new_empty());
        }
        WanStore{
            pack: Arc::new(pack),
            sprites: sprites,
        }
    }

    pub fn preload_sprite(&mut self, sprite_id: usize) -> Result<()> {
        if sprite_id >= self.pack.len() {
            bail!("the sprite id does not exist !");
        }
        match &self.sprites[sprite_id].state {
            PreLoadState::Loading => return Ok(()), //TODO: rather check if it finished, and if there was an error, retry it.
            PreLoadState::Loaded => return Ok(()),
            _ => (),
        }
        let pack = self.pack.clone();
        let handle = thread::spawn(move || {
            let mut file = pack.get_file(sprite_id);
            if is_px(&mut file) {
                Ok(WanImg::new_from_bytes(decompress_px(file)?)?)
            } else {
                Ok(WanImg::new_from_bytes(file)?)
            }
        });
        self.sprites[sprite_id].set_status_loading(handle);
        Ok(())
    }

    pub fn get_sprite(&mut self, renderer: &mut SdlCanvas<SdlWindow>, sprite_id: usize) -> Result<Rc<WanSprite>> {
        if sprite_id >= self.pack.len() {
            bail!("the sprite id does not exist !");
        };
        match &self.sprites[sprite_id].state {
            PreLoadState::NotLoading => self.preload_sprite(sprite_id)?,
            _ => (),
        };

        match &self.sprites[sprite_id].state {
            PreLoadState::Loading => {
                let content = self.sprites[sprite_id].join()?;
                self.sprites[sprite_id].set_result(WanSprite::new_from_wan(content, renderer)?);
            },
            _ => (),
        };

        Ok(self.sprites[sprite_id].get_result()?)
    }
}

pub struct WanHandler {
    sprite: Rc<WanSprite>,
    frame: usize,
    animation: usize,
    looped_at_least_1: bool,
    animation_len: usize,
    animation_loaded: bool,
    with_shadow: bool,
}

impl WanHandler {
    pub fn new(sprite: Rc<WanSprite>, with_shadow: bool) -> WanHandler {
        WanHandler{
            sprite: sprite,
            frame: 0,
            animation: 0,
            looped_at_least_1: false,
            animation_len: 1,
            animation_loaded: false,
            with_shadow: with_shadow,
        }
    }

    pub fn start_animation(&mut self, animation_id: usize) -> Result<()> {
        if animation_id >= self.sprite.len_animations() {
            bail!("WanHandler::start_anim: impossible to set an animation, as it doesn't exist.");
        };
        self.animation_len = self.sprite.len_anim(animation_id).chain_err(|| "WanHandler::draw_next_frame: Impossible to get the lenght of an animation")?;
        self.animation = animation_id;
        self.animation_loaded = true;
        self.looped_at_least_1 = false;
        self.frame = 0;
        Ok(())
    }

    pub fn draw_next_frame(&mut self, canvas: &mut Canvas, coord: &(i32,i32), scale: f32) -> Result<()> {
        //TODO: use the real time between two call to this function
        if !self.animation_loaded {
            error!("no animation is loaded for a WanHandler !!! playing the first frame of the first animation.");
        };
        if self.frame >= std::u16::MAX as usize {
            self.frame = 0;
            self.looped_at_least_1 = true;
            error!("the number of frame of an animation has gone to it's maximum!!! That should never happen, as it should return to 0 when the animation end (or crash for any other reason while drawing previous frame) !!! Restarting it anyway")
        }
        self.sprite.draw_animation(canvas, self.animation, self.frame as u16, self.with_shadow, coord, scale).chain_err(|| "WanHandler::draw_next_frame: Impossible to draw a sprite")?;
        self.frame += 1;
        if self.frame >= self.animation_len {
            self.looped_at_least_1 = true;
            self.frame = 0;
        };
        Ok(())
    }

    pub fn is_finished(&self) -> bool {
        if self.looped_at_least_1 {
            true
        } else {
            false
        }
    }

    pub fn len_animations(&self) -> usize {
        self.sprite.len_animations()
    }
}

pub struct WanImage {
    texture: sdl2::render::Texture,
    width: f32,
    height: f32,
}

impl WanImage {
    fn new(mut data: Vec<u8>, width: u32, height: u32, renderer: &mut SdlCanvas<SdlWindow>) -> Result<WanImage> {
        let surface = Surface::from_data(&mut data,
            width,
            height,
            width * 4,
            PixelFormatEnum::ABGR8888)?; //TODO: see if there isn't an already existing image -> Surface

        let texture = renderer.create_texture_from_surface(&surface)?;
        Ok(WanImage {
            texture: texture,
            width: width as f32,
            height: height as f32,
        })
    }
    fn draw(&self, canvas: &mut Canvas, coord: &(i32,i32), scale: f32, flip: (bool, bool)) -> Result<()> {
        if flip.0 || flip.1 {
            canvas.get_renderer().copy_ex(
                &self.texture,
                None,
                Some(Rect::new(coord.0,coord.1,(self.width*scale) as u32,(self.height*scale) as u32)),
                0.0,
                None,
                flip.0,
                flip.1,
            )?;
        } else {
            canvas.get_renderer().copy(
                &self.texture,
                None,
                Some(Rect::new(coord.0,coord.1,(self.width*scale) as u32,(self.height*scale) as u32)),
            )?;
        }
        Ok(())
    }
}

pub struct WanSprite {
    images: Vec<WanImage>,
    meta_frames: MetaFrameStore,
    animations: AnimStore,
}

impl WanSprite {
    pub fn new_from_wan(wan: WanImg, renderer: &mut SdlCanvas<SdlWindow>) -> Result<WanSprite> {
        let mut images = Vec::new();

        for img in &wan.image_store.images {
            let data = img.img.clone().into_raw();
            let image = WanImage::new(data, img.img.width(), img.img.height(), renderer).chain_err(|| "imposible to load an image")?;
            images.push(image);
        }

        Ok(WanSprite{
            images: images,
            meta_frames: wan.meta_frame_store,
            animations: wan.anim_store,
        })
    }

    pub fn draw_image(&self, canvas: &mut Canvas, image_id: usize, coord: &(i32,i32), scale: f32, flip: (bool, bool)) -> Result<()> {
        if image_id >= self.images.len() {
            bail!("the image id is superior to the lenght of the image list.");
        };
        self.images[image_id].draw(canvas, coord, scale, flip).chain_err(|| "impossible to draw a sprite")?;
        Ok(())
    }

    pub fn draw_meta_frame_group(&self, canvas: &mut Canvas, meta_frame_group_id: usize, coord: &(i32,i32), scale: f32) -> Result<()> {
        if meta_frame_group_id >= self.meta_frames.meta_frame_groups.len() {
            bail!("the meta-frame id is superior to the lenght of the meta-frames list");
        };
        let meta_frames = &self.meta_frames.meta_frames;
        for meta_frame_id in &self.meta_frames.meta_frame_groups[meta_frame_group_id].meta_frames_id {
            let meta_frame = &meta_frames[*meta_frame_id];
            let image_id = meta_frame.image_index;
            let offset_x = ((meta_frame.offset_x as f32) * scale) as i32;
            let offset_y = ((meta_frame.offset_y as f32) * scale) as i32;
            self.draw_image(canvas, image_id, &(coord.0+offset_x, coord.1+offset_y), scale, (meta_frame.h_flip, meta_frame.v_flip)).chain_err(|| "impossible to draw a image while drawing a metaframe")?;
        }
        Ok(())
    }

    pub fn draw_animation(&self, canvas: &mut Canvas, animation_id: usize, frame_number: u16, with_shadow: bool, coord: &(i32,i32), scale: f32) -> Result<()> {
        if animation_id >= self.animations.len() {
            bail!("the animation id is superior to the number of animation");
        };
        let mut frame_id = None;
        let mut frame_actual_number: u16 = 0;
        for actual_frame_id in 0..self.animations.animations[animation_id].len() {
            frame_actual_number += self.animations.animations[animation_id].frames[actual_frame_id].duration as u16;
            if frame_actual_number > frame_number {
                frame_id = Some(actual_frame_id);
                break
            };
        }

        match frame_id {
            None => bail!("the frame number is too high, and is not found in the animation."),
            Some(id) => {

                let frame = &self.animations.animations[animation_id].frames[id];
                let coord_x = coord.0 + (((frame.offset_x as f32) * scale) as i32);
                let coord_y = coord.1 + (((frame.offset_y as f32) * scale) as i32);
                //draw shadow
                if with_shadow {
                    let shadow_x = coord.0 + (((frame.shadow_offset_x as f32) * scale) as i32);
                    let shadow_y = coord.0 + (((frame.shadow_offset_y as f32) * scale) as i32);
                    let shadow_coeff = (10.0*scale) as u32;
                    canvas.draw_rect((255,0,255), (shadow_x-(shadow_coeff as i32), shadow_y-(shadow_coeff as i32), shadow_coeff*2, shadow_coeff*2));
                };
                self.draw_meta_frame_group(canvas, frame.frame_id as usize, &(coord_x, coord_y), scale).chain_err(|| "impossible to draw a sprite animation meta-frame")?;
            },
        }
        Ok(())
    }

    pub fn len_animations(&self) -> usize {
        self.animations.len()
    }

    pub fn len_anim(&self, animation_id: usize) -> Result<usize> {
        if animation_id >= self.len_animations() {
            bail!("impossible to get the lenght of animation, as the given animation id does not exist.");
        };
        let mut frame_number = 0;
        for actual_frame_id in 0..self.animations.animations[animation_id].len() {
            frame_number += self.animations.animations[animation_id].frames[actual_frame_id].duration as usize;
        }
        Ok(frame_number)
    }

    pub fn len_meta_frame_group(&self) -> usize {
        self.meta_frames.meta_frame_groups.len()
    }
}
