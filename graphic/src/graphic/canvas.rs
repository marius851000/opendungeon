use sdl2::video::Window as SdlWindow;
use sdl2::render::Canvas as SdlCanvas;
use sdl2::pixels::Color;
use sdl2::rect::Rect;

pub struct Canvas<'a> {
    pub renderer: &'a mut SdlCanvas<SdlWindow>,
    size: (u32,u32),
}

impl<'a> Canvas<'a> {
    pub fn new(renderer: &'a mut SdlCanvas<SdlWindow>, size: (u32,u32)) -> Canvas<'a> {
        Canvas {
            renderer: renderer,
            size: size,
        }
    }

    pub fn present(&mut self) {
        self.renderer.present();
    }

    pub fn fill(&mut self, color:(u8,u8,u8)) {
        self.renderer.set_draw_color(Color::RGB(color.0, color.1, color.2));
        self.renderer.fill_rect(Rect::new(0,0,self.size.0, self.size.1)).unwrap();
    }

    pub fn draw_rect(&mut self, color:(u8,u8,u8), coord:(i32,i32,u32,u32)) {
        self.renderer.set_draw_color(Color::RGB(color.0, color.1, color.2));
        self.renderer.fill_rect(Rect::new(coord.0,coord.1,coord.2, coord.3)).unwrap();
    }

    pub fn get_renderer(&mut self) -> &mut SdlCanvas<SdlWindow> {
        self.renderer
    }
}
