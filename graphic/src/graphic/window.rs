use crate::graphic::Canvas;
use opendungeon_graphic::errors;

pub struct Window {
    _sdl_context: sdl2::Sdl,
    _video_subsystem: sdl2::VideoSubsystem,
    renderer: sdl2::render::Canvas<sdl2::video::Window>,
    size: (u32,u32),
    pub event_pump: sdl2::EventPump,
}


impl Window {
    pub fn new(default_dimension: (u32,u32), title: &'static str) -> errors::Result<Window> {
        debug!("initializing the window");

        let sdl_context = sdl2::init()?;
        let video_subsystem = sdl_context.video()?;
        let event_pump = sdl_context.event_pump()?;

        let sdl_window = video_subsystem.window(title, default_dimension.0, default_dimension.1)
            .position_centered()
            .build()?;

        let renderer = sdl_window.into_canvas().present_vsync().build()?;

        Ok(Window {
            event_pump: event_pump,
            _video_subsystem: video_subsystem,
            _sdl_context: sdl_context,
            renderer: renderer,
            size: default_dimension,
        })
    }

    pub fn get_render_canvas(&mut self) -> Canvas {
        Canvas::new(&mut self.renderer, self.size)
    }
}
