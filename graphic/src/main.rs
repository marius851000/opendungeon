#![recursion_limit = "1024"]
extern crate sdl2;
extern crate opendungeon_common;
extern crate opendungeon_rom_cpack;
extern crate opendungeon_rom_pkdpx;
extern crate opendungeon_rom_wan;
extern crate ndsrom;
#[macro_use]
extern crate log;
extern crate env_logger;
#[macro_use]
extern crate error_chain;

pub mod graphic;

use self::graphic::{Window};
use self::graphic::wan::{WanStore, WanHandler};
use ndsrom::NdsRom;
use sdl2::event::Event;
use opendungeon_rom_cpack::CPack;
use sdl2::keyboard::Keycode;
use std::path::PathBuf;


use opendungeon_graphic::errors::*;




use crate::graphic::Canvas;
struct WanDebug {
    sprite: WanHandler,
    animation: usize,
    looped: bool,
    have_animation: bool,
}

impl WanDebug {
    pub fn new(mut sprite: WanHandler) -> Result<WanDebug> {
        let have_animation: bool;
        if sprite.len_animations() == 0 {
            have_animation = false;
        } else {
            sprite.start_animation(0).unwrap();
            have_animation = true;
        };
        Ok(WanDebug {
            sprite: sprite,
            animation: 0,
            looped: !have_animation,
            have_animation: have_animation,
        })
    }

    pub fn draw_next_frame(&mut self, canvas: &mut Canvas, coord: &(i32,i32), scale: f32) -> Result<()> {
        if !self.have_animation {
            return Ok(()) //TODO: Error ?
        };
        self.sprite.draw_next_frame(canvas, coord, scale)?;
        if self.sprite.is_finished() {
            self.animation += 1;
            if self.animation >= self.sprite.len_animations() {
                self.animation = 0;
                self.looped = true;
            }
            self.sprite.start_animation(self.animation)?;
        };
        Ok(())
    }

    pub fn is_finished(&self) -> bool {
        self.looped
    }
}




quick_main!(run);

pub fn run() -> Result<()> {
    env_logger::init();

    let rom_folder = "./sky-extracted";

    let rom = NdsRom::new_from_string(rom_folder);
    let mut attack_store = WanStore::new(CPack::new_from_bytes(
        rom.read_file(PathBuf::from("data/MONSTER/m_attack.bin"))?
    )?);

    let mut monster_store = WanStore::new(CPack::new_from_bytes(
        rom.read_file(PathBuf::from("data/MONSTER/monster.bin"))?
    )?);

    let mut ground_store = WanStore::new(CPack::new_from_bytes(
        rom.read_file(PathBuf::from("data/MONSTER/m_ground.bin"))?
    )?);

    let mut window = Window::new((800,800), "test").chain_err(|| "impossible to initiate Window")?;


    let mut attack_sprite = WanDebug::new(WanHandler::new(
        attack_store.get_sprite(&mut window.get_render_canvas().get_renderer(), 0)?, true
    ))?;
    attack_store.preload_sprite(1)?;
    attack_store.preload_sprite(2)?;
    attack_store.preload_sprite(3)?;

    let mut monster_sprite = WanDebug::new(WanHandler::new(
        monster_store.get_sprite(&mut window.get_render_canvas().get_renderer(), 0)?, false
    ))?;
    monster_store.preload_sprite(1)?;
    monster_store.preload_sprite(2)?;
    monster_store.preload_sprite(3)?;

    let mut ground_sprite = WanDebug::new(WanHandler::new(
        ground_store.get_sprite(&mut window.get_render_canvas().get_renderer(), 0)?, false
    ))?;
    ground_store.preload_sprite(1)?;
    ground_store.preload_sprite(2)?;
    ground_store.preload_sprite(3)?;

    let mut loop_nb: u128 = 0;

    let mut monster_id = 0;

    'mainloop: loop {
        loop_nb += 1;
        trace!("loop number {}", loop_nb);
        for event in window.event_pump.poll_iter() {
            match event {
                Event::Quit {..} |
                Event::KeyDown { keycode: Some(Keycode::Escape), .. } => {
                    break 'mainloop
                },
                _ => {}
            }
        }

        let mut canvas = window.get_render_canvas();
        canvas.get_renderer().clear();
        canvas.fill((0,0,0));

        if !attack_sprite.is_finished() {
            attack_sprite.draw_next_frame(&mut canvas, &(200, 200), 5.0).chain_err(|| "run: impossible to draw a sprite animation.")?;
        };

        if !monster_sprite.is_finished() {
            monster_sprite.draw_next_frame(&mut canvas, &(500, 200), 5.0).chain_err(|| "run: impossible to draw a sprite animation.")?;
        };

        if !ground_sprite.is_finished() {
            ground_sprite.draw_next_frame(&mut canvas, &(200, 500), 5.0).chain_err(|| "run: impossible to draw a sprite animation.")?;
        };

        if attack_sprite.is_finished() && monster_sprite.is_finished() && ground_sprite.is_finished() {
            monster_id += 1;
            attack_sprite = WanDebug::new(WanHandler::new(
                attack_store.get_sprite(canvas.get_renderer(), monster_id)?, true
            ))?;
            attack_store.preload_sprite(monster_id+1)?;
            attack_store.preload_sprite(monster_id+2)?;
            attack_store.preload_sprite(monster_id+3)?;


            monster_sprite = WanDebug::new(WanHandler::new(
                monster_store.get_sprite(canvas.get_renderer(), monster_id)?, false
            ))?;
            monster_store.preload_sprite(monster_id+1)?;
            monster_store.preload_sprite(monster_id+2)?;
            monster_store.preload_sprite(monster_id+3)?;

            ground_sprite = WanDebug::new(WanHandler::new(
                ground_store.get_sprite(canvas.get_renderer(), monster_id)?, false
            ))?;
            ground_store.preload_sprite(monster_id+1)?;
            ground_store.preload_sprite(monster_id+2)?;
            ground_store.preload_sprite(monster_id+3)?;



        }


        canvas.present();


    }
    Ok(())
}
